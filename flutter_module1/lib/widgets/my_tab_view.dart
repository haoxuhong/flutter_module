import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/store/index.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/view_util.dart';

class MyTabView extends StatefulWidget {
  final int index;
  final int currentSelected;
  final String text;
  final VoidCallback onTap;

  const MyTabView(
      {Key key, this.index, this.currentSelected, this.text, this.onTap})
      : super(key: key);

  @override
  _MyTabViewState createState() => _MyTabViewState();
}

class _MyTabViewState extends State<MyTabView> {
  @override
  Widget build(BuildContext context) {
    int _currentSelectedIndex =
        Store.value<FunWayDetailModel>(context).currentSelectedIndex;
    MyLogUtil.e('=====${widget.index} ====$_currentSelectedIndex');
    Text textWidget = Text(
      widget.text,
      style: TextStyle(
          color: _currentSelectedIndex == widget.index
              ? Colors.black
              : Colours.color_gray_999,
          fontSize: fontSp(18)),
    );
    return Container(
      child: InkWell(
        onTap: widget.onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            textWidget,
            Visibility(
              visible: _currentSelectedIndex == widget.index,
              child: titleHintLine(),
            )
          ],
        ),
      ),
    );
  }
}
