import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/utils.dart';
import 'package:hxh_base/util/view_util.dart';

/// 自定义AppBar
class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MyAppBar(
      {Key key,
      this.backgroundColor,
      this.gradientStart,
      this.gradientEnd,
      this.isOrientationLeftRight = false,
      this.titleColor = Colors.black,
      this.titleFontSize,
      this.title = '',
      this.centerTitle = '',
      this.actions,
      this.backImg = 'assets/images/ic_back_black.png',
      this.isBack = true,
      this.isWhiteStatusBarFont = true})
      : super(key: key);

  ///标题
  final Color gradientStart; //渐变开始颜色
  final Color gradientEnd; //渐变结束颜色
  final bool isOrientationLeftRight; //默认是从上到下

  final Color backgroundColor;
  final Color titleColor;
  final double titleFontSize;
  final String title;
  final String centerTitle;

  ///左边按钮
  final String backImg;
  final bool isBack;

  ///右边
  final List<Widget> actions;

  ///状态栏字体颜色
  final bool isWhiteStatusBarFont;

  @override
  Widget build(BuildContext context) {
    Color _backgroundColor;

    if (backgroundColor == null) {
      _backgroundColor = Colors.white;
    } else {
      _backgroundColor = backgroundColor;
    }

    /*  final SystemUiOverlayStyle _overlayStyle =
        ThemeData.estimateBrightnessForColor(_backgroundColor) ==
                Brightness.dark
            ? SystemUiOverlayStyle.light
            : SystemUiOverlayStyle.dark;*/

    final Widget back = isBack
        ? IconButton(
            onPressed: () {
              FocusManager.instance.primaryFocus?.unfocus();
              Navigator.maybePop(context);
            },
            padding: EdgeInsets.all(10),
            icon: Image.asset(
              backImg,
              color: titleColor,
            ),
          )
        : SizedBox.shrink();

    final Widget titleWidget = Container(
      alignment: centerTitle.isEmpty ? Alignment.centerLeft : Alignment.center,
      width: Utils.width,
      margin: const EdgeInsets.symmetric(horizontal: 48.0),
      child: Text(
        title.isEmpty ? centerTitle : title,
        style:
            TextStyle(fontSize: titleFontSize ?? fontSp(22), color: titleColor),
      ),
    );

    /*
    final Widget action = actionName.isNotEmpty
        ? Positioned(
            right: 0.0,
            child: Theme(
              data: Theme.of(context).copyWith(
                buttonTheme: const ButtonThemeData(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  minWidth: 60.0,
                ),
              ),
              child: FlatButton(
                child: Text(actionName, key: const Key('actionName')),
                textColor: Colours.color_standard_black,
                highlightColor: Colors.transparent,
                onPressed: onPressed,
              ),
            ),
          )
        : Gaps.empty;*/
    final Widget action = !Utils.isEmpty(actions)
        ? Positioned(
            right: widthDimens(12),
            child: Container(
              alignment: Alignment.centerRight,
              child: Row(
                children: actions,
              ),
            ))
        : SizedBox.shrink();

    Widget appBar = SafeArea(
      child: Stack(
        alignment: Alignment.centerLeft,
        children: <Widget>[titleWidget, back, action],
      ),
    );
// 添加到build方法最后，return之前，通过使用decoration实现颜色的渐变(左右渐变)
    if (gradientStart != null && gradientEnd != null) {
      appBar = Container(
        decoration: BoxDecoration(
          gradient: hxhLinearGradient(
              fromLeftToRight: isOrientationLeftRight,
              colors: [gradientStart, gradientEnd]),
          /*阴影设置
              boxShadow: [
                new BoxShadow(
                  color: Colors.grey[500],
                  blurRadius: 20.0,
                  spreadRadius: 1.0,
                )
              ]*/
        ),
        child: appBar,
      );
    }
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: isWhiteStatusBarFont
          ? SystemUiOverlayStyle.light
          : SystemUiOverlayStyle.dark,
      child: Material(
        // 判断是否使用渐变色
        color: gradientStart != null && gradientEnd != null
            ? Colors.transparent
            : backgroundColor,
        child: appBar,
      ),
    );
  }

  @override
  // Size get preferredSize => Size.fromHeight(Dimens.common_title_size);
  Size get preferredSize =>
      Size.fromHeight(Utils.navigationBarHeight - Utils.topSafeHeight);
}
