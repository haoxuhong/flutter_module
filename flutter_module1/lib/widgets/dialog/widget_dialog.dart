import 'package:flutter/material.dart';

///底部弹框标题
class BottomSheetTitleWidget extends StatelessWidget {
  final Function onTapDelete;

  final EdgeInsetsGeometry padding;
  final String titleText;
  final AlignmentGeometry titleAlignment;
  final Color titleColor;
  final Color iconColor;
  final double fontSize;
  final FontWeight fontWeight;
  final bool isImageHide;
  final double iconSize;

  BottomSheetTitleWidget({
    Key key,
    @required this.onTapDelete,
    this.titleText = "",
    this.titleColor = Colors.black,
    this.fontSize = 18.0,
    this.fontWeight = FontWeight.normal,
    this.titleAlignment = Alignment.centerLeft,
    this.padding,
    this.isImageHide,
    this.iconColor = Colors.black,
    this.iconSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding ?? EdgeInsets.all(0.0),
      height: 48.0,
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            alignment: titleAlignment,
            child: Text(
              titleText,
              style: TextStyle(
                color: titleColor,
                fontSize: fontSize,
                fontWeight: fontWeight,
              ),
            ),
          ),
          Positioned(
              right: 0,
              top: 0,
              bottom: 0,
              child: Offstage(
                offstage: isImageHide ?? true,
                child: Container(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    onTap: () => onTapDelete(),
                    child: Icon(
                      Icons.close,
                      color: iconColor,
                      size: iconSize ?? 22,
                    ),
                    // child: LoadAssetImage('icon_dialog_close',width: 20,height: 20,),
                  ),
                ),
              )),
          Positioned(
              right: 0,
              top: 0,
              bottom: 0,
              child: Offstage(
                offstage: isImageHide ?? true,
                child: Container(
                  child: Icon(
                    Icons.close,
                    color: iconColor,
                    size: iconSize ?? 22,
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
