import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/common/constant.dart';
import 'package:flutter_wisdomscenic/util/directory_util.dart';
import 'package:flutter_wisdomscenic/util/version_utils.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/routers/fluro_navigator.dart';
import 'package:hxh_base/util/image_utils.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/toast_util.dart';

class UpdateDialog extends StatefulWidget {
  final bool isForceUpdate; //是否强制更新 默认不强制更新
  final String mDownloadUrl; //下载地址
  final String mVersionName; //版本号
  final String mUpdateContent; //更新内容

  const UpdateDialog(
      {Key key,
      this.isForceUpdate = false,
      @required this.mDownloadUrl,
      @required this.mVersionName,
      @required this.mUpdateContent})
      : super(key: key);

  @override
  _UpdateDialogState createState() => _UpdateDialogState();
}

class _UpdateDialogState extends State<UpdateDialog> {
  final CancelToken _cancelToken = CancelToken();
  bool _isDownload = false;
  double _value = 0;

  @override
  void dispose() {
    if (!_cancelToken.isCancelled && _value != 1) {
      _cancelToken.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Color primaryColor = Theme.of(context).primaryColor;
    return WillPopScope(
      onWillPop: () async {
        /// 使用false禁止返回键返回，达到强制升级目的
        return !widget.isForceUpdate;
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 120.0,
                  width: 280.0,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0)),
                    image: DecorationImage(
                      image: ImageUtils.getAssetImage('update_head',
                          format: ImageFormat.jpg),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: 280.0,
                  decoration: BoxDecoration(
                      color: context.dialogBackgroundColor,
                      borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(8.0),
                          bottomRight: Radius.circular(8.0))),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('新版本特性', style: TextStyles.textSBlack16),
                            Text(widget.mVersionName,
                                style: TextStyles.textSBlack16),
                          ]),
                      hxhSpace(height: 10),
                      Text(widget.mUpdateContent ?? ''),
                      hxhSpace(height: 15),
                      if (_isDownload)
                        LinearProgressIndicator(
                          backgroundColor: Colours.color_split_line,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(primaryColor),
                          value: _value,
                        )
                      else
                        _buildButton(context),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget _buildButton(BuildContext context) {
    final Color primaryColor = Theme.of(context).primaryColor;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Offstage(
          //当offstage为true，控件隐藏
          offstage: widget.isForceUpdate,
          child: Container(
            width: 110.0,
            height: 36.0,
            child: FlatButton(
              onPressed: () {
                NavigatorUtils.goBack(context);
              },
              textColor: primaryColor,
              color: Colors.transparent,
              disabledTextColor: Colors.white,
              disabledColor: Colours.color_gray_ccc,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
                side: BorderSide(
                  color: primaryColor,
                  width: 0.8,
                ),
              ),
              child: Text(
                '残忍拒绝',
                style: TextStyle(fontSize: fontSp(16)),
              ),
            ),
          ),
        ),
        Container(
          width: 110.0,
          height: 36.0,
          child: FlatButton(
            onPressed: () {
              if (defaultTargetPlatform == TargetPlatform.iOS) {
                NavigatorUtils.goBack(context);
                VersionUtils.jumpAppStore();
              } else {
                setState(() {
                  _isDownload = true;
                });
                _download();
              }
            },
            textColor: Colors.white,
            color: primaryColor,
            disabledTextColor: Colors.white,
            disabledColor: Colours.color_gray_ccc,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
            child: Text(
              '立即更新',
              style: TextStyle(fontSize: fontSp(16)),
            ),
          ),
        )
      ],
    );
  }

  ///下载apk
  Future<void> _download() async {
    try {
      setInitDir(initStorageDir: true);
      await DirectoryUtil.getInstance();
      DirectoryUtil.createStorageDirSync(category: 'Download');
      final String path = DirectoryUtil.getStoragePath(
          fileName: Constant.installName, category: 'Download', format: 'apk');
      MyLogUtil.d('更新地址： $path');

      ///storage/emulated/0/Android/data/com.uweic.flutter_wisdomscenic/files/Download/智慧景区.apk
      final File file = File(path);

      await Dio().download(
        widget.mDownloadUrl,
        file.path,
        cancelToken: _cancelToken,
        onReceiveProgress: (int count, int total) {
          if (total != -1) {
            _value = count / total;
            setState(() {});
            if (count == total) {
              NavigatorUtils.goBack(context);
              VersionUtils.install(path);
            }
          }
        },
      );
    } catch (e) {
      ToastUtil.show('下载失败!');
      print(e);
      setState(() {
        _isDownload = false;
      });
    }
  }
}
