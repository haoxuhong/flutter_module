import 'package:flutter/material.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:flutter_wisdomscenic/widgets/dialog/address_dialog.dart';
import 'package:flutter_wisdomscenic/widgets/dialog/update_dialog.dart';
import 'package:flutter_wisdomscenic/widgets/dialog/widget_dialog.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/toast_util.dart';

///dialog使用示例
class ExampleDialog {
  static void addressDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return AddressDialog(
              onSelected:
                  (province, provinceCode, city, cityCode, county, countyCode) {
                MyLogUtil.d(
                    '$province $provinceCode $city $cityCode $county $countyCode');
                ToastUtil.show(
                    '$province $provinceCode $city $cityCode $county $countyCode');
              },
              title: '选择地区',
              unselectedColor: Colors.blue);
        });
  }

  static void updateDialog(BuildContext context) {
    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (_) => UpdateDialog(
              mDownloadUrl:
                  'https://54a0bf2343ff38bdc347780545bd8c9e.dd.cdntips.com/imtt.dd.qq.com/16891/apk/E664A57DD3EA0540676EFC830BFDDE97.apk',
              // "https://dldir1.qq.com/weixin/android/weixin7014android1660.apk",
              mVersionName: 'v1.0.0',
              mUpdateContent: "到底更新了啥\n到底更新了啥\n到底更新了啥\n到底更新了啥\n到底更新了啥",
            ));
  }

  ///todo 只有标题
  static YYDialog yYListViewDialogListTile(BuildContext context) {
    var yyDialog = YYDialog();
    return yyDialog.build(context)
      ..gravity = Gravity.bottom
      ..width = double.infinity
      ..borderRadius = 4.0
      ..widget(BottomSheetTitleWidget(
        onTapDelete: () {
          yyDialog.dismiss();
          MyLogUtil.d('点击了退出');
        },
        padding: EdgeInsets.only(left: 16, right: 16),
        titleText: '选择门票',
        titleAlignment: Alignment.center,
        isImageHide: false,
      ))
      ..show();
  }
}
