import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class BannerBean {
  // final double height;
  // final double width;
  final String imageUrl;
  final String jumpUrl;

  BannerBean({@required this.imageUrl, @required this.jumpUrl});
}

class HxhBanner extends StatelessWidget {
  final List<BannerBean> bannerList;
  final double bannerHeight;
  final EdgeInsetsGeometry padding;
  final ImageProvider<dynamic> backgroundImage;
  final Alignment paginationAlignment;
  final bool haveCircular;

  const HxhBanner(this.bannerList,
      {Key key,
      this.bannerHeight = 160,
      this.padding,
      this.backgroundImage,
      this.paginationAlignment = Alignment.bottomRight,
      this.haveCircular = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: bannerHeight,
      decoration: backgroundImage == null
          ? null
          : BoxDecoration(
              image: DecorationImage(
                image: backgroundImage,
                fit: BoxFit.fitWidth,
                alignment: Alignment.bottomCenter,
              ),
            ),
      child: _banner(context),
    );
  }

  _banner(BuildContext context) {
    var right = 10 + (padding?.horizontal ?? 0) / 2;
    return Swiper(
      itemCount: bannerList.length,
      autoplay: true,
      itemBuilder: (BuildContext context, int index) {
        return _image(context, bannerList[index]);
      },
      //自定义指示器
      pagination: SwiperPagination(
          alignment: paginationAlignment,
          margin: EdgeInsets.only(right: right, bottom: 10),
          builder: DotSwiperPaginationBuilder(
              color: Colors.white60, // 没选中时的颜色
              activeColor: Colors.white, // 选中时的颜色
              size: 6,
              activeSize: 6)),
    );
  }

  _image(BuildContext context, BannerBean bannerBean) {
    return InkWell(
      onTap: () {
        handleBannerClick(context, bannerBean);
      },
      child: Container(
        padding: padding,
        child: haveCircular
            ? ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(6)),
                child: Image.network(bannerBean.imageUrl, fit: BoxFit.cover),
              )
            : Image.network(bannerBean.imageUrl, fit: BoxFit.cover),
      ),
    );
  }
}

///banner点击跳转
void handleBannerClick(BuildContext context, BannerBean bannerBean) {
  Navigator.pushNamed(context, '/webview',
      arguments: {'url': bannerBean.jumpUrl});
}
