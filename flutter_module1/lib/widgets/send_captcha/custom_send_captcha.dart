import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/mvp/base_page.dart';
import 'package:flutter_wisdomscenic/widgets/send_captcha/send_captcha_presenter.dart';
import 'package:hxh_base/res/resources.dart';

import 'send_captcha_iView.dart';

///自定义带倒计时的发送验证码按钮
class CustomSendCaptchaBtn extends StatefulWidget {
  final int second; // 倒计时秒数
  final double height;
  final double width;
  final String textStr;
  final String mobile;

  const CustomSendCaptchaBtn(
      {Key key,
      this.second: 60,
      this.height,
      this.width,
      this.textStr: '发送验证码',
      @required this.mobile})
      : super(key: key);

  @override
  _CustomSendCaptchaBtnState createState() => _CustomSendCaptchaBtnState();
}

class _CustomSendCaptchaBtnState extends State<CustomSendCaptchaBtn>
    with BasePageMixin<CustomSendCaptchaBtn, SendCaptchaPresenter>
    implements SendCaptchaIMvpView {
  bool _clickable = true;

  /// 当前秒数
  int _currentSecond;
  StreamSubscription _subscription;

  @override
  void dispose() {
    _subscription?.cancel();
    super.dispose();
  }

  void _getVCode() {
    if (widget.mobile.isEmpty) {
      showToast('手机号不能为空');
      return;
    }
    Map<String, dynamic> params = {};
    params['key'] = widget.mobile;
    _presenter.sendCaptcha(params);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: widget.height ?? heightDimens(38),
        width: widget.width ?? widthDimens(83),
        child: MaterialButton(
            onPressed: _clickable ? _getVCode : null,
            padding: EdgeInsets.zero,
            textColor: Colors.white,
            color: _clickable ? primary : Colours.color_gray_ccc,
            disabledTextColor: Colors.white,
            disabledColor: Colours.color_gray_ccc,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2.0),
            ),
            child: Text(_clickable ? widget.textStr : "$_currentSecond s后重发",
                style: TextStyle(fontSize: fontSp(14)))));
  }

  SendCaptchaPresenter _presenter;

  @override
  SendCaptchaPresenter createPresenter() {
    _presenter = SendCaptchaPresenter();
    return _presenter;
  }

  @override
  void sendCaptchaSuccess() {
    showToast('发送短信成功');
    setState(() {
      _currentSecond = widget.second;
      _clickable = false;
    });
    _subscription = Stream.periodic(const Duration(seconds: 1), (int i) => i)
        .take(widget.second)
        .listen((int i) {
      setState(() {
        _currentSecond = widget.second - i - 1;
        _clickable = _currentSecond < 1;
      });
    });
  }
}
