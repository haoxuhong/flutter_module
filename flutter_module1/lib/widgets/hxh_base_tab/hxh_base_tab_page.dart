import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_wisdomscenic/common/constant.dart';
import 'package:flutter_wisdomscenic/enum/enum.dart';
import 'package:flutter_wisdomscenic/mvp/base_page.dart';
import 'package:flutter_wisdomscenic/mvp/base_presenter.dart';
import 'package:flutter_wisdomscenic/net/error_handle.dart';
import 'package:flutter_wisdomscenic/widgets/state_layout.dart';
import 'package:hxh_base/util/log_util.dart';

import 'hxh_base_tab_iView.dart';

///通用的list列表
abstract class HxhBaseTabPageState<M, P extends BasePresenter,
        T extends StatefulWidget> extends State<T>
    with AutomaticKeepAliveClientMixin<T>, BasePageMixin<T, P>
    implements HxhBaseTabView<M> {
  int pageIndex = 1;

  List<M> dataList = [];

  bool _isLoadComplete = false;
  StateType _stateType = StateType.loading;

  @override
  bool get wantKeepAlive => true;

  get contentChild;

  ///获取对应页码的数据
  getData(int pageIndex, RefreshType refreshType);

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      initData();
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    MyLogUtil.d('是否加载完成：$_isLoadComplete');
    return dataList.length == 0
        ? StateLayout(
            _stateType,
            onTap: () {
              initData();
            },
          )
        : Container(
            color: Colors.white,
            child: EasyRefresh(
                footer: BallPulseFooter(enableHapticFeedback: true),
                header: MaterialHeader(enableHapticFeedback: true),
                onLoad: _isLoadComplete
                    ? null
                    : () async {
                        pageIndex++;
                        _loadData(RefreshType.LOAD_MORE);
                      },
                onRefresh: () async {
                  initData();
                },
                child: contentChild),
          );
  }

  ///初始化数据
  void initData() {
    _initParams();
    _loadData(RefreshType.REFRESH);
  }

  void _initParams() {
    pageIndex = 1;
    _isLoadComplete = false;
    _stateType = StateType.loading;
    dataList?.clear();
  }

  void _refreshLayout() {
    setState(() {});
  }

  void _loadData(RefreshType refreshType) {
    getData(pageIndex, refreshType);
  }

  @override
  void getDataSuccess(List<M> data, RefreshType refreshType) {
    if (refreshType == RefreshType.DEFAULT ||
        RefreshType.REFRESH == refreshType) {
      if (data.length == 0) {
        _stateType = StateType.empty;
      } else {
        _stateType = StateType.success;
      }
    }
    if (data.length < Constant.pageSize) {
      _isLoadComplete = true;
    }
    if (data.length != 0)
      dataList = [...dataList, ...data]; //合成一个新数组  等价于 dataList.addAll(data);
    _refreshLayout();
  }

  @override
  void getDataFailHintPage(int code, String mag) {
    _initParams();
    if (ExceptionHandle.socket_error == code ||
        ExceptionHandle.net_error == code) {
      //网络异常
      _stateType = StateType.failNet;
    } else {
      _stateType = StateType.fail;
    }

    _refreshLayout();
  }
}
