import 'package:flutter_wisdomscenic/enum/enum.dart';
import 'package:flutter_wisdomscenic/mvp/mvps.dart';

///HxhBaseTabView
abstract class HxhBaseTabView<M> implements IMvpView {
  void getDataFailHintPage(int code, String mag);

  void getDataSuccess(List<M> data, RefreshType refreshType);
}
