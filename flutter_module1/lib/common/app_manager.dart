import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_wisdomscenic/common/constant.dart';
import 'package:flutter_wisdomscenic/net/dio_utils.dart';
import 'package:flutter_wisdomscenic/net/intercept.dart';
import 'package:flutter_wisdomscenic/res/strings.dart';
import 'package:flutter_wisdomscenic/routers/routers.dart';
import 'package:fluwx/fluwx.dart';
import 'package:hxh_base/util/log_util.dart';

class AppManager {
  static void initApp() {
    initEasyRefresh();

    ///日志打印管理
    initLogManager();
    initDio();
    Routes.initRoutes();
    initWXLogin();
  }

  ///微信登录
  static void initWXLogin() async {
    registerWxApi(
      appId: 'xxxxx', //添加自己项目的
      doOnAndroid: true,
      doOnIOS: true,
      universalLink: 'xxxx', //添加自己项目的
    );

    /* var result = await isWeChatInstalled;
    MyLogUtil.d("是否安装微信 $result");*/
  }

  ///日志打印管理   App运行在Release环境时，inProduction为true；当App运行在Debug和Profile环境时，inProduction为false
  static void initLogManager() {
    if (Constant.inProduction) {
      MyLogUtil.init(false);
    } else {
      MyLogUtil.init(true);
    }
  }

  static void initDio() {
    final List<Interceptor> interceptors = [];

    /// 统一添加身份验证请求头
    interceptors.add(AuthInterceptor());

    /// 打印Log(生产模式去除)
    if (!Constant.inProduction) {
      interceptors.add(LoggingInterceptor());
    }

    /// 适配数据(根据自己的数据结构，可自行选择添加)
    interceptors.add(AdapterInterceptor());

    setInitDio(
      baseUrl: Constant.baseUrl,
      interceptors: interceptors,
    );
  }

  /// 设置EasyRefresh的默认样式
  static void initEasyRefresh() {
    EasyRefresh.defaultHeader = ClassicalHeader(
        enableInfiniteRefresh: false,
        refreshText: S().pullToRefresh,
        refreshReadyText: S().releaseToRefresh,
        refreshingText: S().refreshing,
        refreshedText: S().refreshed,
        refreshFailedText: S().refreshFailed,
        noMoreText: S().noMore,
        infoText: S().updateAt,
        infoColor: Colors.black45);
    EasyRefresh.defaultFooter = ClassicalFooter(
        loadText: S().pushToLoad,
        loadReadyText: S().releaseToLoad,
        loadingText: S().loading,
        loadedText: S().loaded,
        loadFailedText: S().loadFailed,
        noMoreText: S().noMore,
        infoText: S().updateAt,
        infoColor: Colors.black45);
  }
}
