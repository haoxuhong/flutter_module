import 'package:flutter_wisdomscenic/common/constant.dart';
import 'package:flutter_wisdomscenic/generated/json/base/json_convert_content.dart';
import 'package:hxh_base/util/log_util.dart';

class BaseEntity<T> {
  int code;
  String message;
  T data;

  BaseEntity(this.code, this.message, this.data);

  BaseEntity.fromJson(Map<String, dynamic> json) {
    code = json[Constant.code] as int;
    message = json[Constant.message] as String;
    if (json.containsKey(Constant.data)) {
      data = _generateOBJ<T>(json[Constant.data]);
    }
  }

  T _generateOBJ<T>(Object json) {
    MyLogUtil.d('data类型：${T.toString()}');
    if (T.toString() == 'String') {
      return json.toString() as T;
    } else if (T.toString() == 'Map<dynamic, dynamic>') {
      return json as T;
    } else if (T.toString() == 'bool') {
      return json as T;
    } else {
      /// List类型数据由fromJsonAsT判断处理
      return JsonConvert.fromJsonAsT<T>(json);
    }
  }
}
