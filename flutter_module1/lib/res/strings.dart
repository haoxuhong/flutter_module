

class S {
  ///首页底部导航栏
  static const String goAlong = '随行';
  static const String funWay = '趣途';
  static const String tour = '游拍';
  static const String mine = '我的';

  ///刷新默认文字ClassicalHeader
  final String pullToRefresh = '拉动刷新';
  final String releaseToRefresh = '释放刷新';
  final String refreshing = '正在刷新...';
  final String refreshed = '刷新完成';
  final String refreshFailed = '刷新失败';
  final String noMore = '没有更多数据';
  final String updateAt = '更新于 %T';
  ///刷新默认文字ClassicalFooter
  final String pushToLoad = '拉动加载';
  final String releaseToLoad = '释放加载';
  final String loading = '正在加载...';
  final String loaded = '加载完成';
  final String loadFailed = '加载失败';

  ///底部导航栏图标地址
  static const String goAlong_image_url_selected = 'tabs/ic_goAlong_selected';
  static const String goAlong_image_url_unselected = 'tabs/ic_goAlong_unselected';
  static const String funWay_image_url_selected = 'tabs/ic_funWay_selected';
  static const String funWay_image_url_unselected = 'tabs/ic_funWay_unselected';
  static const String tour_image_url_selected = 'tabs/ic_tour_selected';
  static const String tour_image_url_unselected = 'tabs/ic_tour_unselected';
  static const String mine_image_url_selected = 'tabs/ic_mine_selected';
  static const String mine_image_url_unselected = 'tabs/ic_mine_unselected';

  static const String hint_layout_no_data = '亲，暂无数据';
  static const String hint_layout_error_request = '亲，请求出错了';
  static const String hint_layout_error_network = '亲，没有网络了';
  static const String click_me_to_retry = '点击屏幕，重新加载';

  static const String common_internet_error = '网络异常,请重新连接网络';

  static const String common_submit = '提交';
  static const String common_v_code = '获取验证码';
  static const String common_submit_success = '提交成功';
  static const String common_cancel = '取消';
  static const String common_confirm = '确认';

  static const String common_name = '姓名';
  static const String common_name_hint = '请输入姓名';
  static const String common_name_empty_hint = '姓名不能为空';
  static const String common_id_card = '身份证号';
  static const String common_id_card_hint = '请输入身份证号';
  static const String common_id_card_empty_hint = '身份证号不能为空';
  static const String common_id_card_error_hint = '请输入正确的身份证号';
  static const String common_phone = '电话';
  static const String common_phone_hint = '请输入电话';
  static const String common_phone_empty_hint = '电话不能为空';
  static const String common_phone_error_hint = '请输入正确的电话号码';
  static const String common_sex = '性别';
  static const String common_sex_hint = '请输入性别';
  static const String common_sex_empty_hint = '性别不能为空';
  static const String common_photo = '照片';
  static const String common_photo_empty_hint = '还没上传照片，请上传照片';
}
