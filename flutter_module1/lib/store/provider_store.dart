import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'index.dart';

class Store {
  //  初始化
  static init({Widget child}) {
    /// 返回多个状态
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => FunWayDetailModel()),
      ],
      child: child,
    );
  }

  //  通过Provider.value<T>(context)获取状态数据     listen: 为true时重新构建（build刷新界面），为false时不重新构建（不刷新界面）
  static T value<T>(BuildContext context, {bool listen: true}) {
    return Provider.of<T>(context, listen: listen);
  }

  //  通过Consumer获取状态数据  widget build太频繁时使用
  static Consumer connect<T>(
      {Function(BuildContext context, T value, Widget child) builder,
      Widget child}) {
    return Consumer<T>(builder: builder, child: child);
  }

  //  通过Consumer获取状态数据
  static Consumer2 connect2<A, B>(
      {Function(BuildContext context, A value, B value2, Widget child) builder,
      Widget child}) {
    return Consumer2<A, B>(builder: builder, child: child);
  }
}
