import 'package:flutter_wisdomscenic/generated/json/base/json_convert_content.dart';
import 'package:flutter_wisdomscenic/generated/json/base/json_field.dart';

class FileUploadEntity with JsonConvert<FileUploadEntity> {
	String fullPath;
	String fileName;
	String filepath;
	String bucketname;
	@JSONField(name: "upload-date")
	String uploadDate;
}
