import 'package:flutter/material.dart';
import 'package:flutter_custom_dialog/flutter_custom_dialog.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_wisdomscenic/common/app_manager.dart';
import 'package:flutter_wisdomscenic/module/tabs/tabs_page.dart';
import 'package:flutter_wisdomscenic/routers/routers.dart';
import 'package:oktoast/oktoast.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  MyApp() {
    AppManager.initApp();
  }

  @override
  Widget build(BuildContext context) {
    //1、初始化context
    YYDialog.init(context);
    //设置适配尺寸 (填入设计稿中设备的屏幕尺寸) 此处假如设计稿是按iPhone6的尺寸设计的(iPhone6 750*1334)
    ScreenUtil.init(const BoxConstraints());

    return ScreenUtilInit(
      designSize: const Size(375, 667),
      allowFontScaling: false,
      builder: () => OKToast(
          child: MaterialApp(
            title: '有为景区',
//              showPerformanceOverlay: true, //显示性能标签
            // 去除右上角debug的标签
            debugShowCheckedModeBanner: false,
//              checkerboardRasterCacheImages: true,
//              showSemanticsDebugger: true, // 显示语义视图
//              checkerboardOffscreenLayers: true, // 检查离屏渲染
            theme: ThemeData(
              primaryColor: Colors.pink,
            ),
            // home: SplashPage(),
            home: TabsPage(),
            // home: TabsPage(),
            onGenerateRoute: Routes.router.generator,
            navigatorKey: navigatorKey,
          ),

          /// Toast 配置
          /// 全局设置隐藏之前的属性,这里设置后,每次当你显示新的 toast 时,旧的就会被关闭
          dismissOtherOnShow: true,
          //自己添加的设置，Flutter Deer开源项目没设置这个字段
          backgroundColor: Colors.black54,
          textPadding:
              const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
          radius: 20.0.r,
          position: ToastPosition.bottom),
    );
  }
}
