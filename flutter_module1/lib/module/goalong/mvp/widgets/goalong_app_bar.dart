import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bmflocation/bdmap_location_flutter_plugin.dart';
import 'package:flutter_bmflocation/flutter_baidu_location_android_option.dart';
import 'package:flutter_bmflocation/flutter_baidu_location_ios_option.dart';
import 'package:flutter_wisdomscenic/routers/my_navigator.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/image_utils.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/toast_util.dart';
import 'package:hxh_base/util/utils.dart';
import 'package:hxh_base/util/view_util.dart';

class GoalongAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  _GoalongAppBarState createState() => _GoalongAppBarState();

  @override
  Size get preferredSize =>
      Size.fromHeight(Utils.navigationBarHeight - Utils.topSafeHeight);
}

class _GoalongAppBarState extends State<GoalongAppBar> {
  @override
  Widget build(BuildContext context) {
    final Widget searchWidget = Container(
        margin: EdgeInsets.symmetric(horizontal: widthDimens(10)),
        decoration: BoxDecoration(
            color: Color(0xFFFF6F5C),
            borderRadius: BorderRadius.all(Radius.circular(radiusDimens(18)))),
        padding: EdgeInsets.symmetric(
            horizontal: widthDimens(10), vertical: heightDimens(6)),
        child: Row(children: <Widget>[
          Icon(Icons.search, color: Colors.white),
          Text('搜索目的地/景点',
              style: TextStyle(fontSize: fontSp(12), color: Colors.white))
        ]));

    final Widget actionWidget = Container(
        margin: EdgeInsets.only(right: widthDimens(12)),
        child: Image.asset(
          ImageUtils.getImgPath('ic_goalong_news'),
          width: widthDimens(25), //75
          height: heightDimens(22), //66
          fit: BoxFit.scaleDown,
          alignment: Alignment.centerRight,
        ));
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Material(
        // 判断是否使用渐变色
        color: Colors.transparent,
        child: Container(
          decoration: BoxDecoration(
            gradient: hxhLinearGradient(colors: [
              Colours.color_app_bar_gradient_start,
              Colours.color_app_bar_gradient_middle
            ]),
          ),
          child: SafeArea(
            child: Container(
              height: Utils.navigationBarHeight - Utils.topSafeHeight,
              child: Row(
                children: <Widget>[
                  LocationWidget(),
                  Expanded(child: searchWidget),
                  InkWell(
                      onTap: () {
                        MyNavigatorUtils.goHintPage(context, '消息');
                      },
                      child: actionWidget)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class LocationWidget extends StatefulWidget {
  @override
  _LocationWidgetState createState() => _LocationWidgetState();
}

class _LocationWidgetState extends State<LocationWidget> {
  String _cityStr = '北京';
  LocationFlutterPlugin _locationPlugin = new LocationFlutterPlugin();

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(milliseconds: 500), () {
      _startLocation();
    });

    _locationPlugin.onResultCallback().listen((Map<String, Object> result) {
      MyLogUtil.d('百度地图定位信息_loationResult：$result');

      if (result.isEmpty) {
        ToastUtil.show('定位失败');
        _stopLocation();
        return;
      }

      var cityResult = result['city'];
      var latitude = result['latitude'];
      var longitude = result['longitude'];
      MyLogUtil.d(
          '百度地图定位信息_loationResult：$cityResult latitude：$latitude  longitude：$longitude');

      if (cityResult != null) {
        cityResult = Utils.citySplit(cityResult);
        setState(() {
          _cityStr = cityResult;
        });
      } else {
        ToastUtil.show('定位失败');
      }
      _stopLocation();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _stopLocation();
  }

  /// 设置android端和ios端定位参数
  void _setLocOption() {
    /// android 端设置定位参数
    BaiduLocationAndroidOption androidOption = new BaiduLocationAndroidOption();
    androidOption.setCoorType("bd09ll"); // 设置返回的位置坐标系类型
    androidOption.setIsNeedAltitude(true); // 设置是否需要返回海拔高度信息
    androidOption.setIsNeedAddres(true); // 设置是否需要返回地址信息
    androidOption.setIsNeedLocationPoiList(true); // 设置是否需要返回周边poi信息
    androidOption.setIsNeedNewVersionRgc(true); // 设置是否需要返回最新版本rgc信息
    androidOption.setIsNeedLocationDescribe(true); // 设置是否需要返回位置描述
    androidOption.setOpenGps(true); // 设置是否需要使用gps
    androidOption.setLocationMode(LocationMode.Hight_Accuracy); // 设置定位模式
    androidOption.setScanspan(1000); // 设置发起定位请求时间间隔

    Map androidMap = androidOption.getMap();

    /// ios 端设置定位参数
    BaiduLocationIOSOption iosOption = new BaiduLocationIOSOption();
    iosOption.setIsNeedNewVersionRgc(true); // 设置是否需要返回最新版本rgc信息
    iosOption.setBMKLocationCoordinateType(
        "BMKLocationCoordinateTypeBMK09LL"); // 设置返回的位置坐标系类型
    iosOption.setActivityType("CLActivityTypeAutomotiveNavigation"); // 设置应用位置类型
    iosOption.setLocationTimeout(10); // 设置位置获取超时时间
    iosOption.setDesiredAccuracy("kCLLocationAccuracyBest"); // 设置预期精度参数
    iosOption.setReGeocodeTimeout(10); // 设置获取地址信息超时时间
    iosOption.setDistanceFilter(100); // 设置定位最小更新距离
    iosOption.setAllowsBackgroundLocationUpdates(true); // 是否允许后台定位
    iosOption.setPauseLocUpdateAutomatically(true); //  定位是否会被系统自动暂停

    Map iosMap = iosOption.getMap();

    _locationPlugin.prepareLoc(androidMap, iosMap);
  }

  /// 启动定位
  void _startLocation() {
    if (null != _locationPlugin) {
      _setLocOption();
      _locationPlugin.startLocation();
    }
  }

  /// 停止定位
  void _stopLocation() {
    if (null != _locationPlugin) {
      _locationPlugin.stopLocation();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
            padding:
                EdgeInsets.only(left: widthDimens(12), right: widthDimens(5)),
            child: Image.asset(
              ImageUtils.getImgPath('ic_goalong_location'),
              width: widthDimens(14),
              height: heightDimens(16),
              fit: BoxFit.scaleDown,
              alignment: Alignment.centerLeft,
            )),
        InkWell(
          onTap: () {},
          child: Container(
            constraints: BoxConstraints(maxWidth: widthDimens(62)),
            child: Text(_cityStr,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: fontSp(17), color: Colors.white)),
          ),
        )
      ],
    );
  }
}
