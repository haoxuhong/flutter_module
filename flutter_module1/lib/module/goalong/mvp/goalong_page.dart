import 'package:flutter/material.dart';

import 'widgets/goalong_app_bar.dart';

///随行
class GoAlongPage extends StatefulWidget {
  @override
  _GoAlongPageState createState() => _GoAlongPageState();
}

class _GoAlongPageState extends State<GoAlongPage>
    with
        AutomaticKeepAliveClientMixin<GoAlongPage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: GoalongAppBar(),
      body: Container(
        child: Center(child: Text('随行')),
      ),
    );
  }
}
