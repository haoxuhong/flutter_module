import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_wisdomscenic/routers/my_navigator.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/image_utils.dart';
import 'package:hxh_base/util/utils.dart';
import 'package:hxh_base/util/view_util.dart';

class TourAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Material(
        color: Colors.white,
        child: Container(
          child: SafeArea(
            child: Container(
              // height: Utils.navigationBarHeight - Utils.topSafeHeight,
              decoration: BoxDecoration(
                  // color:Colors.white,
                  border: borderLine(context)),
              child: Stack(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      MyNavigatorUtils.goHintPage(context, '搜索');
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        ImageUtils.getImgPath('ic_tour_search'),
                        width: radiusDimens(24),
                        height: radiusDimens(24),
                        fit: BoxFit.scaleDown,
                        alignment: Alignment.center,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      MyNavigatorUtils.goHintPage(context, '消息');
                    },
                    child: Container(
                      alignment: Alignment.centerRight,
                      margin: EdgeInsets.only(right: widthDimens(12)),
                      child: Image.asset(
                        ImageUtils.getImgPath('ic_tour_news'),
                        width: radiusDimens(24),
                        height: radiusDimens(24),
                        fit: BoxFit.scaleDown,
                        alignment: Alignment.centerRight,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize =>
      Size.fromHeight(Utils.navigationBarHeight - Utils.topSafeHeight);
}
