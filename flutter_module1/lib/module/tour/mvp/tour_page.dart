import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/module/tour/mvp/widgets/tour_app_bar.dart';

class TourPage extends StatefulWidget {
  @override
  _TourPageState createState() => _TourPageState();
}

class _TourPageState extends State<TourPage>
    with
        AutomaticKeepAliveClientMixin<TourPage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: TourAppBar(),
      body: Container(
        child: Center(child: Text('游拍')),
      ),
    );
  }
}
