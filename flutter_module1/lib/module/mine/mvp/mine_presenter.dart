import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/module/mine/mvp/mine_iView.dart';
import 'package:flutter_wisdomscenic/mvp/base_page_presenter.dart';
import 'package:flutter_wisdomscenic/net/net.dart';

class MinePresenter extends BasePagePresenter<MineIMvpView> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      initDate();
    });
  }

  initDate() {
    asyncRequestNetwork<Map>(Method.get, url: '链接', onSuccess: (data) {
      view.getUserInfoSuccess();
    }, onError: (_, __) {
      print('$_, $__');
    });
  }
}
