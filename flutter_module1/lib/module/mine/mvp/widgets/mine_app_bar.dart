import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/utils.dart';
import 'package:hxh_base/util/view_util.dart';
import 'package:hxh_base/widgets/image_load_view.dart';

class MineAppBar extends StatelessWidget implements PreferredSizeWidget {
  final VoidCallback onExitCallback;

  const MineAppBar({Key key, @required this.onExitCallback}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Material(
        // 判断是否使用渐变色
        color: Colors.transparent,
        child: Container(
          decoration: BoxDecoration(
            gradient: hxhLinearGradient(colors: [
              Colours.color_app_bar_gradient_start,
              Colours.color_app_bar_mine_gradient_middle
            ]),
          ),
          child: SafeArea(
            child: Container(
              height: Utils.navigationBarHeight - Utils.topSafeHeight,
              child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    Center(
                      child: Text(
                        '我的',
                        style: TextStyles.textWhite22,
                      ),
                    ),
                    Positioned(
                      right: widthDimens(12),
                      child: InkWell(
                        onTap: onExitCallback,
                        child: ImageLoadView(
                          'ic_mine_exit',
                          width: widthDimens(22),
                          height: heightDimens(22),
                          imageType: ImageType.assets,
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize =>
      Size.fromHeight(Utils.navigationBarHeight - Utils.topSafeHeight);
}
