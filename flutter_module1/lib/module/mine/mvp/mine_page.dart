import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/module/mine/mvp/mine_iView.dart';
import 'package:flutter_wisdomscenic/module/mine/mvp/mine_presenter.dart';
import 'package:flutter_wisdomscenic/module/mine/mvp/widgets/mine_app_bar.dart';
import 'package:flutter_wisdomscenic/mvp/base_page.dart';

class MinePage extends StatefulWidget {
  @override
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage>
    with
        AutomaticKeepAliveClientMixin<MinePage>,
        BasePageMixin<MinePage, MinePresenter>
    implements MineIMvpView {
  @override
  bool get wantKeepAlive => true;

  MinePresenter _presenter;

  @override
  MinePresenter createPresenter() {
    _presenter = MinePresenter();
    return _presenter;
  }

  @override
  void getUserInfoSuccess() {}

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MineAppBar(
        onExitCallback: () {},
      ),
      body: Container(
        child: Center(child: Text('我的')),
      ),
    );
  }
}
