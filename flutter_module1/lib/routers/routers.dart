import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/common/hint_page.dart';
import 'package:flutter_wisdomscenic/module/funway/fun_way_router.dart';
import 'package:flutter_wisdomscenic/module/goalong/goalong_router.dart';
import 'package:flutter_wisdomscenic/module/mine/mine_router.dart';
import 'package:flutter_wisdomscenic/module/tabs/tabs_page.dart';
import 'package:flutter_wisdomscenic/module/tabs/webview_page.dart';
import 'package:hxh_base/routers/i_router.dart';

class Routes {
  static String tabs = '/tabs';
  static String webViewPage = '/webView';
  static String reportPage = '/reportPage';
  static String hintPage = '/hintPage';

  static final List<IRouterProvider> _listRouter = [];

  static final FluroRouter router = FluroRouter();

  static void initRoutes() {
    router.define(tabs,
        handler: Handler(
            handlerFunc:
                (BuildContext context, Map<String, List<String>> params) =>
                    TabsPage()));

    router.define(webViewPage, handler: Handler(handlerFunc: (_, params) {
      final String title = params['title']?.first;
      final String url = params['url']?.first;
      return WebViewPage(title: title, url: url);
    }));

    router.define(hintPage, handler: Handler(handlerFunc: (_, params) {
      final String title = params['title'].first;
      final String hintText = params['hintText']?.first;
      return HintPage(
        title: title,
        hintText: hintText,
      );
    }));

    _listRouter.clear();

    /// 各自路由由各自模块管理，统一在此添加初始化
    _listRouter.add(GoalongRouter());
    _listRouter.add(FunWayRouter());
    _listRouter.add(MineRouter());

    /// 初始化路由
    _listRouter.forEach((routerProvider) {
      routerProvider.initRouter(router);
    });
  }
}
