import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';

/// fluro的路由跳转工具类
class NavigatorUtils {
  /// replace：true 就是将 原 页面给移除掉了，这点后退键的时候就不会再出现原页面
  static void push(BuildContext context, String path,
      {bool replace = false, bool clearStack = false}) {
    unfocus();
    FluroRouter().navigateTo(context, path,
        replace: replace,
        clearStack: clearStack,
        transition: TransitionType.native);
  }

  ///带页面返回值的
  static void pushResult(
      BuildContext context, String path, Function(Object) function,
      {bool replace = false, bool clearStack = false}) {
    unfocus();
    FluroRouter()
        .navigateTo(context, path,
            replace: replace,
            clearStack: clearStack,
            transition: TransitionType.native)
        .then((Object result) {
      // 页面返回result为null
      if (result == null) {
        return;
      }
      function(result);
    }).catchError((dynamic error) {
      print('$error');
    });
  }

  /// 返回
  static void goBack(BuildContext context) {
    unfocus();
    Navigator.pop(context);
  }

  /// 带参数返回
  static void goBackWithParams(BuildContext context, Object result) {
    unfocus();
    Navigator.pop<Object>(context, result);
  }

  static void unfocus() {
    // 使用下面的方式，会触发不必要的build。
    // FocusScope.of(context).unfocus();
    // https://github.com/flutter/flutter/issues/47128#issuecomment-627551073
    FocusManager.instance.primaryFocus?.unfocus();
  }

  ///fluro 不支持传中文,需转换
  static String encodeComponent(String component) {
    return Uri.encodeComponent(component);
  }
}
