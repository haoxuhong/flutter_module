import 'package:flutter/material.dart';

///主色调
const MaterialColor primary = const MaterialColor(
  0xFFFF3231,
  const <int, Color>{50: const Color(0xffff9db5)},
);

class Colours {
  ///灰色
  static const Color color_gray_666 = Color(0xFF666666);
  static const Color color_gray_999 = Color(0xFF999999);
  static const Color color_gray_ccc = Color(0xFFcccccc);
  static const Color color_gray_EEE = Color(0xFFEEEEEE);

  static const Color color_black = Color(0xFF000000);
  static const Color color_standard_black = Color(0xFF333333);

  static const Color color_app_bar_gradient_start = Color(0xFFFF3231);
  static const Color color_app_bar_gradient_middle = Color(0xFFFF542E);
  static const Color color_app_bar_mine_gradient_middle = Color(0xFFff4430);
  static const Color color_app_bar_gradient_end = Color(0xFFFF5E1F);

  // 按钮 禁用状态下字体颜色
  static const Color text_disabled = Color(0xFFD4E2FA);

  // 按钮 禁用状态下背景颜色
  static const Color button_disabled = Color(0xFF96BBFA);

  //   分割线的颜色
  static const Color color_split_line = Color(0xFFE9E9E9);

  static const Color color_default_shadow = Color(0x80DCE7FA);
  static const Color color_ticket_tab_bar_shadow = Color(0x24FF3B3A);
  static const Color color_ticket_red = Color(0xFFFF002B);
}

extension ThemeExtension on BuildContext {
  Color get backgroundColor => Colors.white;

  Color get dialogBackgroundColor => Theme.of(this).canvasColor;
}
