import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///间距
SizedBox hxhSpace({double height: 1, double width: 1}) {
  return SizedBox(height: height.h, width: width.w);
}

///高
double heightDimens(double height) {
  return height.h;
}

///宽
double widthDimens(double width) {
  return width.w;
}

///弧角
double radiusDimens(double radius) {
  return radius.r;
}

///字体
double fontSp(double fontSize) {
  return fontSize.sp;
}
