import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

///border线
borderLine(BuildContext context,
    {bottom: true,
    top: false,
    right: false,
    left: false,
    double width: 0.5,
    Color lineColor}) {
  BorderSide borderSide =
      BorderSide(width: width, color: lineColor ?? Color(0xFFEEEEEE));
  return Border(
      left: left ? borderSide : BorderSide.none,
      right: right ? borderSide : BorderSide.none,
      bottom: bottom ? borderSide : BorderSide.none,
      top: top ? borderSide : BorderSide.none);
}

///标题下面的提示线
Widget titleHintLine({double height: 4, double width: 42}) {
  return Container(
    height: height.h,
    width: width.w,
    decoration: BoxDecoration(
        gradient: hxhLinearGradient(
            fromLeftToRight: true,
            colors: [Color(0xFFFFD6D1), Color(0xFFFF4F4F)]),
        borderRadius: BorderRadius.all(Radius.circular(25.r))),
  );
}

///线性渐变,默认黑色 默认从上到下
hxhLinearGradient({bool fromLeftToRight = false, List<Color> colors}) {
  return LinearGradient(
      begin: fromLeftToRight ? Alignment.centerLeft : Alignment.topCenter,
      end: fromLeftToRight ? Alignment.centerRight : Alignment.bottomCenter,
      colors: colors ??
          [
            Colors.transparent,
            Colors.black12,
            Colors.black26,
            Colors.black38,
            Colors.black45,
            Colors.black54,
          ]);
}

///底部阴影
BoxDecoration bottomBoxShadow(BuildContext context) {
  return BoxDecoration(color: Colors.white, boxShadow: [
    BoxShadow(
        color: Colors.grey[100],
        offset: Offset(0, 5), //xy轴偏移
        blurRadius: 5.0, //阴影模糊程度
        spreadRadius: 1 //阴影扩散程度
        )
  ]);
}
