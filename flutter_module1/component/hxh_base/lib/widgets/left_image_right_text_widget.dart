import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/widgets/image_load_view.dart';

///左边图片右边文字
class LeftImageRightTextWidget extends StatelessWidget {
  final String imageUrl;
  final String textStr;
  final TextStyle textStyle;
  final ImageType imageType;
  final BoxFit fit;
  final double width; //图片宽度
  final double height; //图片高度
  final double paddingWidth; //整体宽度
  final double paddingHeight; //整体高度
  final double spaceWidth; //中间的间距

  const LeftImageRightTextWidget({
    Key key,
    @required this.imageUrl,
    @required this.textStr,
    @required this.textStyle,
    this.imageType: ImageType.assets,
    this.width,
    this.height,
    this.fit: BoxFit.scaleDown,
    this.spaceWidth,
    this.paddingWidth,
    this.paddingHeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: paddingWidth ?? 0, vertical: paddingHeight ?? 0),
      child: Row(
        children: <Widget>[
          ImageLoadView(
            imageUrl,
            width: width,
            height: height,
            imageType: imageType,
            fit: fit,
          ),
          SizedBox(width: spaceWidth ?? widthDimens(4)),
          Text(
            textStr,
            style: textStyle,
          )
        ],
      ),
    );
  }
}
