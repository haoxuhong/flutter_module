import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/utils.dart';

enum CodeInputType {
  squareBox, //方框类型
  underLine, //下划线类型
  circle, //圆形
  round //圆角
}

enum KeyboardType { none, number, letter }

///验证码输入框
class CodeInputText extends StatefulWidget {
  final int length;
  final CodeInputType type;
  final bool obscureText; //将输入框设置为密码框

  final Function(String value) callBack;

  final KeyboardType keyboardType;

  final double radius;
  final double height;
  final double width;

  const CodeInputText({
    Key key,
    this.height,
    @required this.width,
    this.length: 6,
    this.type: CodeInputType.round,
    this.callBack,
    this.keyboardType: KeyboardType.number,
    this.obscureText: false,
    this.radius: 2,
  })  : assert(length >= 4 && length <= 6),
        super(key: key);

  @override
  createState() => _CodeInputTextState();
}

class _CodeInputTextState extends State<CodeInputText> {
  TextEditingController _controller = TextEditingController();

  Pattern pattern = RegExp("[a-z,0-9,A-Z]");

  @override
  void initState() {
    super.initState();

    _controller.addListener(() {
      setState(() {});
    });

    if (widget.keyboardType == KeyboardType.letter) {
      pattern = RegExp("[a-z,A-Z]");
    } else if (widget.keyboardType == KeyboardType.number) {
      pattern = RegExp("[0-9]");
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _newCells = [];

    if (widget.type == CodeInputType.round) {
      for (int i = 1; i <= widget.length; i++) {
        _newCells.add(SquareInputCell(
          isFocused: _controller.text.length == i - 1,
          text: _controller.text.length >= i
              ? _controller.text.substring(i - 1, i)
              : '',
          obscureText: widget.obscureText,
          radius: widget.radius,
          cellWidth: widget.length != i
              ? widget.width / widget.length - 3
              : widget.width / widget.length,
          rightSpace: widget.length != i ? 3.0 : 0.0,
        ));
      }
    } else if (widget.type == CodeInputType.squareBox) {
      for (int i = 1; i <= widget.length; i++) {
        _newCells.add(SquareInputCell(
            isFocused: _controller.text.length == i - 1,
            text: _controller.text.length >= i
                ? _controller.text.substring(i - 1, i)
                : '',
            obscureText: widget.obscureText,
            cellWidth: widget.length != i
                ? widget.width / widget.length - 3
                : widget.width / widget.length,
            rightSpace: widget.length != i ? 3.0 : 0.0,
            radius: 0.0));
      }
    } else if (widget.type == CodeInputType.circle) {
      for (int i = 1; i <= widget.length; i++) {
        _newCells.add(CircleInputCell(
          isFocused: _controller.text.length == i - 1,
          text: _controller.text.length >= i
              ? _controller.text.substring(i - 1, i)
              : '',
          obscureText: widget.obscureText,
          cellWidth: widget.length != i
              ? widget.width / widget.length - 3
              : widget.width / widget.length,
          rightSpace: widget.length != i ? 3.0 : 0.0,
        ));
      }
    } else {
      for (int i = 1; i <= widget.length; i++) {
        _newCells.add(UnderlineInputCell(
          isFocused: _controller.text.length == i - 1,
          text: _controller.text.length >= i
              ? _controller.text.substring(i - 1, i)
              : '',
          obscureText: widget.obscureText,
          cellWidth: widget.length != i
              ? widget.width / widget.length - 3
              : widget.width / widget.length,
          rightSpace: widget.length != i ? 3.0 : 0.0,
        ));
      }
    }

    return Container(
        height: widget.height ?? heightDimens(38),
        width: widget.width,
        child: Stack(children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: _newCells),
          Opacity(
              opacity: 0,
              child: TextField(
                controller: _controller,
                inputFormatters: [FilteringTextInputFormatter.allow(pattern)],
                maxLength: widget.length,
                autofocus: true,
                keyboardType: widget.keyboardType == KeyboardType.number
                    ? TextInputType.number
                    : TextInputType.text,
                onChanged: (value) {
                  widget.callBack(value);
                  if (value.length == widget.length) {
                    /// 失去焦点，隐藏键盘
                    Utils.hideKeyboard(context);
                  }
                },
                obscureText: widget.obscureText,
              ))
        ]));
  }
}

class SquareInputCell extends StatefulWidget {
  final bool isFocused;
  final String text;
  final bool obscureText; //将输入框设置为密码框
  final double radius;
  final double cellWidth;
  final Color bgColor;
  final double rightSpace;

  const SquareInputCell({
    Key key,
    this.isFocused,
    this.text,
    this.obscureText: false,
    this.radius,
    this.bgColor: Colors.white30,
    this.cellWidth, //默认45
    this.rightSpace,
  }) : super(key: key);

  @override
  createState() => _SquareInputCellState();
}

class _SquareInputCellState extends State<SquareInputCell> {
  /// 聚焦时的光标颜色，实现光标闪烁
  Color cursorColor;
  Timer timer;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      cursorColor = Colors.transparent;
      if (mounted) {
        setState(() {});
      }
      Future.delayed(Duration(milliseconds: 500), () {
        cursorColor = Theme.of(context).primaryColor;
        if (mounted) {
          setState(() {});
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.cellWidth ?? widthDimens(45),
      margin: EdgeInsets.only(right: widget.rightSpace),
      decoration: BoxDecoration(
          color: widget.bgColor,
          borderRadius: BorderRadius.circular(widget.radius)),
      child: widget.isFocused
          ? Container(
              color: cursorColor,
              child: hxhSpace(width: 1, height: 20),
            )
          : widget.obscureText
              ? widget.text == ''
                  ? SizedBox()
                  : Icon(Icons.brightness_1, size: radiusDimens(20))
              : Text(widget.text ?? '', style: TextStyles.textWhite14),
      alignment: Alignment.center,
    );
  }
}

class UnderlineInputCell extends StatelessWidget {
  final bool isFocused;
  final String text;
  final bool obscureText;
  final double cellWidth;
  final Color bgColor;
  final double rightSpace;
  const UnderlineInputCell(
      {Key key,
      this.isFocused,
      this.text,
      this.obscureText: false,
      this.cellWidth,
      this.bgColor: Colors.white30,
      this.rightSpace})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: cellWidth ?? widthDimens(45),
      margin: EdgeInsets.only(right: rightSpace),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: isFocused ? Theme.of(context).primaryColor : bgColor,
                  width: 2))),
      child: obscureText
          ? text == ''
              ? SizedBox()
              : Icon(Icons.brightness_1, size: radiusDimens(20))
          : Text(text ?? '', style: TextStyles.textWhite14),
      alignment: Alignment.center,
    );
  }
}

class CircleInputCell extends StatelessWidget {
  final bool isFocused;
  final String text;
  final bool obscureText;
  final double cellWidth;
  final Color bgColor;
  final double rightSpace;
  CircleInputCell(
      {Key key,
      this.isFocused,
      this.text,
      this.obscureText: false,
      this.cellWidth,
      this.bgColor: Colors.white30,
      this.rightSpace})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: cellWidth ?? widthDimens(45),
      margin: EdgeInsets.only(right: rightSpace),
      decoration: BoxDecoration(
        color: isFocused ? Theme.of(context).primaryColor : bgColor,
        shape: BoxShape.circle,
      ),
      child: obscureText
          ? text == ''
              ? SizedBox()
              : Icon(Icons.brightness_1, size: radiusDimens(20))
          : Text(text ?? '', style: TextStyles.textWhite14),
      alignment: Alignment.center,
    );
  }
}
