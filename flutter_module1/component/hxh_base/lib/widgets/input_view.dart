import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/widgets/text_view.dart';

class TextFieldItem extends StatelessWidget {
  final TextEditingController controller;
  final String title;
  final String hintText;
  final TextInputType keyboardType;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final int maxLines;
  final int maxLength;
  final TextAlign textAlign;
  final Color bgColor;
  final double height;
  final bool isRequired;

  const TextFieldItem({
    Key key,
    this.controller,
    @required this.title,
    this.keyboardType: TextInputType.text,
    this.hintText: "",
    this.focusNode,
    this.nextFocusNode,
    this.maxLines: 1,
    this.maxLength,
    this.textAlign: TextAlign.start,
    this.bgColor: Colors.transparent,
    this.height,
    this.isRequired: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Row child = Row(
        crossAxisAlignment: maxLines == 1
            ? CrossAxisAlignment.center
            : CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(right: widthDimens(16)),
              child:
                  // Text(title, style: TextStyles.textSBlack14)),
                  RichTextRequired(title: title, isRequired: isRequired)),
          Expanded(
              child: TextField(
                  textAlign: textAlign,
                  maxLength: maxLength,
                  cursorColor: primary,
                  style: TextStyles.textSBlack14,
                  maxLines: maxLines,
                  focusNode: focusNode,
                  keyboardType: keyboardType,
                  inputFormatters: (keyboardType == TextInputType.number ||
                          keyboardType == TextInputType.phone)
                      ? [FilteringTextInputFormatter.allow(RegExp("[0-9]"))]
                      : keyboardType ==
                              TextInputType.numberWithOptions(decimal: true)
                          ? [UsNumberTextInputFormatter()]
                          : [FilteringTextInputFormatter.deny(RegExp(""))],
                  controller: controller,
                  onEditingComplete: nextFocusNode == null
                      ? null
                      : () =>
                          FocusScope.of(context).requestFocus(nextFocusNode),
                  decoration: InputDecoration(
                      /*contentPadding:
                          EdgeInsets.symmetric(vertical: Dimens.hgap_dp16),*/
                      hintText: hintText,
                      counterText: "",
                      border: InputBorder.none,
                      hintStyle: TextStyles.text99914)))
        ]);
    return Container(
        color: bgColor,
        height: maxLines == 1
            ? (height ?? heightDimens(50))
            : maxLines * (height ?? heightDimens(50)),
        margin: EdgeInsets.symmetric(horizontal: widthDimens(16)),
        width: double.infinity,
        /*  decoration: BoxDecoration(
          border: Border(
            bottom: Divider.createBorderSide(context, width: 0.6.w),
          ),
        ),*/
        child: child);
  }
}

/// 只允许输入小数
class UsNumberTextInputFormatter extends TextInputFormatter {
  static const defaultDouble = 0.001;

  static double strToFloat(String str, [double defaultValue = defaultDouble]) {
    try {
      return double.parse(str);
    } catch (e) {
      return defaultValue;
    }
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String value = newValue.text;
    int selectionIndex = newValue.selection.end;
    if (value == ".") {
      value = "0.";
      selectionIndex++;
    } else if (value != "" &&
        value != defaultDouble.toString() &&
        strToFloat(value, defaultDouble) == defaultDouble) {
      value = oldValue.text;
      selectionIndex = oldValue.selection.end;
    }
    return TextEditingValue(
        text: value,
        selection: TextSelection.collapsed(offset: selectionIndex));
  }
}

class CustomTextField extends StatefulWidget {
  final TextEditingController controller;
  final int maxLength;
  final bool autoFocus;
  final TextInputType keyboardType;
  final String hintText;
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final bool isInputPwd;
  final Function getVCode;
  final Duration duration;
  final Widget prefixIcon;
  final TextStyle hintTextStyle;

  CustomTextField({
    Key key,
    @required this.controller,
    this.maxLength: 16,
    this.autoFocus: false,
    this.keyboardType: TextInputType.text,
    this.hintText: "",
    this.focusNode,
    this.nextFocusNode,
    this.isInputPwd: false,
    this.getVCode,
    this.prefixIcon,
    this.hintTextStyle,
    this.duration: const Duration(seconds: 60),
  }) : super(key: key);

  @override
  createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _isShowPwd = false;
  bool _isShowDelete = true;

  final StreamController<Model> _streamController = StreamController<Model>();

  /// 倒计时秒数
  int second;

  /// 当前秒数
  int s;

  @override
  void initState() {
    super.initState();
    second = widget.duration.inSeconds;

    //监听输入改变
    widget.controller.addListener(() {
      setState(() {
        _isShowDelete = widget.controller.text.isEmpty;
      });
    });
  }

  Timer _timer;

  @override
  void dispose() {
    _streamController?.close();
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.centerRight, children: <Widget>[
      TextField(
          focusNode: widget.focusNode,
          maxLength: widget.maxLength,
          style: TextStyle(textBaseline: TextBaseline.alphabetic),
          cursorColor: primary,
          //光标颜色
          /// 键盘动作按钮点击之后执行的代码： 光标切换到指定的输入框
          onEditingComplete: widget.nextFocusNode == null
              ? null
              : () => FocusScope.of(context).requestFocus(widget.nextFocusNode),
          obscureText: widget.isInputPwd ? !_isShowPwd : false,
          autofocus: widget.autoFocus,
          controller: widget.controller,
          textInputAction: TextInputAction.done,
          keyboardType: widget.keyboardType,
          // 数字、手机号限制格式为0到9(白名单)， 密码限制不包含汉字（黑名单）
          inputFormatters: (widget.keyboardType == TextInputType.number ||
                  widget.keyboardType == TextInputType.phone)
              ? [FilteringTextInputFormatter.allow(RegExp("[0-9]"))]
              : [FilteringTextInputFormatter.deny(RegExp("[\u4e00-\u9fa5]"))],
          decoration: InputDecoration(
              border: OutlineInputBorder(borderSide: BorderSide.none),
              contentPadding: EdgeInsets.zero,
              hintText: widget.hintText,
              hintStyle: widget.hintTextStyle ?? TextStyles.text99914,
              counterText: "",
              prefixIcon: widget.prefixIcon)),
      Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Visibility(
            visible: !_isShowDelete,
            child: InkWell(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: Icon(Icons.close, size: 18.0),
                onTap: () => setState(() => widget.controller.text = ""))),
        Visibility(
            visible: widget.isInputPwd,
            child: Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: InkWell(
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    child: Icon(
                        _isShowPwd
                            ? Icons.visibility_sharp
                            : Icons.visibility_off_sharp,
                        size: 18.0),
                    onTap: () {
                      setState(() => _isShowPwd = !_isShowPwd);
                    }))),
        Visibility(
            visible: widget.getVCode != null,
            child: Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Container(
                    height: 26.0,
                    width: 76.0,
                    child: StreamBuilder<Model>(
                        stream: _streamController.stream,
                        initialData: Model(second: second, isClick: true),
                        builder: (BuildContext context,
                            AsyncSnapshot<Model> snapshot) {
                          return FlatButton(
                              onPressed: snapshot.data.isClick
                                  ? () {
                                      _streamController.sink.add(Model(
                                          second: second, isClick: false));
                                      widget.getVCode();
                                      s = second;

                                      _timer = Timer.periodic(
                                          Duration(seconds: 1), (timer) {
                                        if (s <= 0) {
                                          _streamController.sink.add(Model(
                                              second: --s, isClick: true));
                                          _timer?.cancel();
                                        } else {
                                          _streamController.sink.add(Model(
                                              second: --s, isClick: false));
                                        }
                                      });
                                    }
                                  : null,
                              padding: const EdgeInsetsDirectional.only(
                                  start: 8.0, end: 8.0),
                              textColor: Colors.blueAccent,
                              color: Colors.transparent,
                              disabledTextColor: Colors.white,
                              disabledColor: Colours.color_gray_ccc,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1.0),
                                  side: BorderSide(
                                      color: snapshot.data.isClick
                                          ? Colors.blueAccent
                                          : Colours.color_gray_ccc,
                                      width: 0.8)),
                              child: Text(
                                  snapshot.data.isClick
                                      ? "获取验证码"
                                      : "（${snapshot.data.second} s）",
                                  style: TextStyle(fontSize: fontSp(12))));
                        }))))
      ])
    ]);
  }
}

class Model {
  bool isClick;
  int second;

  Model({this.isClick, this.second});
}
