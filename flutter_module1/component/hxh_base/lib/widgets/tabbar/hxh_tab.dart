import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

///顶部tab切换组件
class HxhTab extends StatelessWidget {
  final List<Widget> tabs;
  final TabController controller;
  final double fontSize;
  final double indicatorWeight;
  final bool isScrollable;
  final Color labelColor;
  final Color unselectedLabelColor;
  final ValueChanged<int> onTap;

  const HxhTab(this.tabs,
      {Key key,
      this.controller,
      this.fontSize = 18,
      this.unselectedLabelColor = Colours.color_gray_999,
      this.labelColor,
      this.isScrollable = false,
      this.onTap,
      this.indicatorWeight = 2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
        controller: controller,
        isScrollable: isScrollable,
        labelColor: labelColor ?? Color(0xFFFF3231),
        unselectedLabelColor: unselectedLabelColor,
        labelPadding: EdgeInsets.zero,
        labelStyle: TextStyle(fontSize: fontSize),
        onTap: onTap,
        indicatorColor: labelColor ?? Color(0xFFFF3231),
        indicatorWeight: indicatorWeight,
        tabs: tabs);
  }
}
