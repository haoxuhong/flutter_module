import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

class RichTextRequired extends StatelessWidget {
  final bool isRequired;
  final String title;
  final double titleFontSize;
  final Color titleColor;
  final Color hintColor;

  const RichTextRequired(
      {Key key,
      this.isRequired,
      this.title,
      this.titleFontSize,
      this.titleColor: Colours.color_standard_black,
      this.hintColor: Colours.color_ticket_red})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          style: DefaultTextStyle.of(context).style,
          children: <InlineSpan>[
            TextSpan(
              text: title,
              style: TextStyle(
                fontSize: titleFontSize ?? fontSp(14),
                color: titleColor,
              ),
            ),
            TextSpan(
              text: isRequired == true ? '*' : '',
              style: TextStyle(
                fontSize: titleFontSize ?? fontSp(14),
                color: hintColor,
              ),
            )
          ]),
    );
  }
}
