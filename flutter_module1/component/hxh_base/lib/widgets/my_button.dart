import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hxh_base/res/resources.dart';

class MyButton extends StatelessWidget {
  const MyButton({
    Key key,
    this.text = '',
    @required this.onPressed,
    this.backgroundColor,
    this.textColor,
    this.textDisabledColor,
    this.buttonDisabledColor,
    this.height,
    this.width,
    this.fontSize,
  }) : super(key: key);

  final String text;
  final Color backgroundColor; //背景颜色
  final Color textColor; //字体颜色
  final Color textDisabledColor; //禁用状态下字体颜色
  final Color buttonDisabledColor; //禁用状态下背景颜色
  final double height; //高
  final double width; //宽
  final double fontSize; //字体大小
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.zero,
      onPressed: onPressed,
      textColor: Colors.white,
      color: backgroundColor ?? Color(0xFFF4F4F4),
      disabledTextColor: textDisabledColor ?? Colours.text_disabled,
      disabledColor: buttonDisabledColor ?? Colours.button_disabled,
      //shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        height: height ?? 48.h,
        width: width ?? double.infinity,
        alignment: Alignment.center,
        child: Text(
          text,
          style: TextStyle(
              color: textColor ?? Colors.white,
              fontSize: fontSize ?? fontSp(18)),
        ),
      ),
    );
  }
}
