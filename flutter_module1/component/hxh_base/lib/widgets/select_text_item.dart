import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

/*使用示例
*  SelectTextItem(
                      leading: Icon(Icons.email, size: 26),
                      title: '邮箱',
                      subTitle: '当时发生的',
                      content: '${userModel.getEmail()}',
                      margin: EdgeInsets.only(left: 16.0, right: 8),
                      onTap: isEdit
                          ? () => pushNewPage(
                                  context,
                                  InputTextPage(
                                    title: '修改邮箱',
                                    content: '${userModel.getEmail()}',
                                    maxLines: 1,
                                    keyboardType: TextInputType.emailAddress,
                                  ), callBack: (value) {
                                userModel.setUser(email: value);
                              })
                          : null,
                      textAlign: TextAlign.right),
* */

///左边图片   左边文字和左边子标题  内容   右箭头（或图片）       点击事件（无点击事件时，隐藏箭头图标）
class SelectTextItem extends StatelessWidget {
  final GestureTapCallback onTap;
  final String title;
  final String content;
  final TextAlign textAlign;
  final TextStyle contentTextStyle;
  final Widget leading;
  final IconData trailing;
  final String subTitle;
  final double height;
  final EdgeInsetsGeometry margin;
  final Color bgColor;
  final Color rightIconColor;
  final TextStyle titleTextStyle;

  const SelectTextItem({
    Key key,
    this.onTap,
    @required this.title,
    this.content: "",
    this.textAlign: TextAlign.end,
    this.contentTextStyle,
    this.leading,
    this.subTitle: "",
    this.height: 55.0,
    this.trailing,
    this.margin,
    this.bgColor,
    this.titleTextStyle,
    this.rightIconColor,
  })  : assert(title != null, height >= 40.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: bgColor ?? Colors.white,
      child: InkWell(
          onTap: onTap,
          child: Container(
              height: height,
              margin:
                  margin ?? EdgeInsets.symmetric(horizontal: widthDimens(8)),
              width: double.infinity,
              child: Row(children: <Widget>[
                Offstage(
                    offstage: leading == null,
                    child: Row(children: <Widget>[
                      leading == null ? SizedBox() : leading,
                      hxhSpace(width: 8)
                    ])),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('${title ?? ""}',
                          style: titleTextStyle ?? TextStyles.textSBlack18,
                          maxLines: 1),
                      Offstage(
                          offstage: subTitle.isEmpty,
                          child: Text(subTitle,
                              style: TextStyles.text99912,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis))
                    ]),
                Expanded(
                    child: Padding(
                        padding: margin ??
                            EdgeInsets.symmetric(horizontal: widthDimens(8)),
                        child: Text("${content ?? ''}",
                            maxLines: 1,
                            textAlign: textAlign,
                            overflow: TextOverflow.ellipsis,
                            style: contentTextStyle ?? TextStyles.text99912))),
                Offstage(
                    offstage: onTap == null,
                    child: Icon(
                      trailing ?? Icons.chevron_right,
                      size: 22.0,
                      color: rightIconColor,
                    ))
              ]))),
    );
  }
}
