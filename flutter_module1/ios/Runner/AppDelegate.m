#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [GeneratedPluginRegistrant registerWithRegistry:self];
  // Override point for customization after application launch.
  // 启动图片延时: 2秒
  [NSThread sleepForTimeInterval:2];
  return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
