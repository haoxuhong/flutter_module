import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/mvp/base_presenter.dart';
import 'package:flutter_wisdomscenic/routers/fluro_navigator.dart';
import 'package:flutter_wisdomscenic/widgets/progress_dialog.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/toast_util.dart';

import '../common/constant.dart';
import 'mvps.dart';

mixin BasePageMixin<T extends StatefulWidget, P extends BasePresenter>
    on State<T> implements IMvpView {
  P? presenter;

  P createPresenter();

  @override
  BuildContext getContext() {
    return context;
  }

  @override
  void uploadImageSuccess(String path) {}

  @override
  void closeProgress() {
    MyLogUtil.d('关闭加载框==》$mounted _isShowDialog: $_isShowDialog');
    if (mounted && _isShowDialog) {
      _isShowDialog = false;
      NavigatorUtils.goBack(context);
    }
  }

  bool _isShowDialog = false;

  @override
  void showProgress({String? hintText = '正在加载...'}) {
    /// 避免重复弹出
    MyLogUtil.d('显示加载框==》$mounted _isShowDialog: $_isShowDialog');
    if (mounted && !_isShowDialog) {
      _isShowDialog = true;
      try {
        showDialog<void>(
          context: context,
          barrierDismissible: false,
          barrierColor: const Color(0x00FFFFFF),
          // 默认dialog背景色为半透明黑色，这里修改为透明（1.20添加属性）
          builder: (_) {
            return WillPopScope(
              onWillPop: () async {
                // 拦截到返回键，证明dialog被手动关闭
                _isShowDialog = false;
                return Future.value(true);
              },
              child: buildProgress(hintText ?? ''),
            );
          },
        );
      } catch (e) {
        /// 异常原因主要是页面没有build完成就调用Progress。
        /// 可以在presenter的initstate方法中进行网络请求
        MyLogUtil.d('========网络请求异常：页面没有build完成就调用Progress');
        print(e);
      }
    }
  }

  @override
  void showToast(String? string,
      {int duration = Constant.toastDefaultShowTime}) {
    ToastUtil.show(string ?? '', duration: duration);
  }

  /// 可自定义Progress
  Widget buildProgress(String hintText) => ProgressDialog(hintText: hintText);

  ///初始化时，在initState()之后立刻调用
  ///当依赖的InheritedWidget rebuild,会触发此接口被调用
  @override
  void didChangeDependencies() {
    presenter?.didChangeDependencies();
    MyLogUtil.d('$T ==> didChangeDependencies');
    super.didChangeDependencies();
  }

  ///当State对象从树中被永久移除时调用；通常在此回调中释放资源
  @override
  void dispose() {
    presenter?.dispose();
    MyLogUtil.d('$T ==> dispose');
    super.dispose();
  }

  ///当State对象从树中被移除时，会调用此回调
  @override
  void deactivate() {
    presenter?.deactivate();
    MyLogUtil.d('$T ==> deactivate');
    super.deactivate();
  }

  ///状态改变的时候会调用该方法,比如调用了setState
  @override
  void didUpdateWidget(T oldWidget) {
    presenter?.didUpdateWidgets<T>(oldWidget);
    MyLogUtil.d('$T ==> didUpdateWidgets');
    super.didUpdateWidget(oldWidget);
  }

  ///当Widget第一次插入到Widget树时会被调用，对于每一个State对象，Flutter framework只会调用一次该回调
  @override
  void initState() {
    MyLogUtil.d('$T ==> initState');
    presenter = createPresenter();
    presenter?.view = this;
    presenter?.initState();
    super.initState();
  }
}
