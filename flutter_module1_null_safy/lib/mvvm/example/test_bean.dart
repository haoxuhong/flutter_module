class TestBean {
  int? count;
  List<BannerModel>? list;

  TestBean({this.count, this.list});

  TestBean.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['list'] != null) {
      list = [];
      json['list'].forEach((v) {
        list!.add(new BannerModel.fromJson(v));
      });
    } else {
      list = null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.list != null) {
      data['list'] = this.list!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BannerModel {
  int? id;
  String? name;
  String? image;
  String? appImage;
  String? url;
  int? status;

  BannerModel(
      {this.id, this.name, this.image, this.url, this.status, this.appImage});

  BannerModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    appImage = json['appImage'];
    url = json['url'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    data['appImage'] = this.appImage;
    data['url'] = this.url;
    data['status'] = this.status;
    return data;
  }
}
