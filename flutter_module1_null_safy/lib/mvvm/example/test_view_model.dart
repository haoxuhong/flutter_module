import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/mvvm/base_page_view_model.dart';
import 'package:flutter_wisdomscenic/mvvm/example/test_bean.dart';

import '../../net/dio_utils.dart';

class TestViewModel extends BasePageViewModel with ChangeNotifier {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      initData();
    });
  }

  TestBean? _bean;

  TestBean? get bean => _bean;

  void initData() {
    asyncRequestNetwork<Map>(Method.post,
        url: 'https://sapi.easyebid.com/auction/banner', onSuccess: (data) {
      _bean = TestBean.fromJson(data as Map<String, dynamic>);
      notifyListeners();
    });
  }
}
