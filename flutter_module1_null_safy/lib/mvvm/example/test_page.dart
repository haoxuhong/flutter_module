import 'package:flutter/material.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:flutter_wisdomscenic/mvvm/base_page.dart';
import 'package:flutter_wisdomscenic/mvvm/example/test_bean.dart';
import 'package:flutter_wisdomscenic/mvvm/example/test_view_model.dart';
import 'package:hxh_base/widgets/image_load_view.dart';
import 'package:provider/provider.dart';

import '../../widgets/my_app_bar.dart';

class TestPage extends StatefulWidget {
  const TestPage({Key? key}) : super(key: key);

  @override
  State<TestPage> createState() => _TestPageState();
}

class _TestPageState extends State<TestPage>
    with BasePageMixin<TestPage, TestViewModel> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyAppBar(centerTitle: '意见反馈'),
      body: ChangeNotifierProvider<TestViewModel>(
          create: (context) => _model,
          builder: (BuildContext context, Widget? child) {
            return Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(4)),
                child: initLayout());
          }),
    );
  }

  late TestViewModel _model;

  @override
  TestViewModel createViewModel() {
    _model = TestViewModel();
    return _model;
  }

  initLayout() {
    return Selector<TestViewModel, TestBean?>(
      selector: (context, homoViewModel) => homoViewModel.bean,
      builder: (context, bannerDataModel, _) {
        if (bannerDataModel == null ||
            bannerDataModel.list == null ||
            bannerDataModel.list!.length == 0)
          return Container(width: 0, height: 0);
        int length = bannerDataModel.list!.length;
        return Container(
          height: 200,
          child: Swiper(
            key: ValueKey(bannerDataModel),
            itemBuilder: (BuildContext context, int index) {
              BannerModel bannerModel = bannerDataModel.list![index];
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: ImageLoadView(
                      (bannerModel.appImage == null ||
                              bannerModel.appImage!.length == 0)
                          ? bannerModel.image!
                          : bannerModel.appImage!,
                      fit: BoxFit.fill,
                      radius: 4),
                ),
              );
            },
            autoplay: true,
            duration: 800,
            itemCount: length,
            pagination: SwiperPagination(
                alignment: Alignment.bottomRight,
                margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                builder: SwiperCustomPagination(
                    builder: (BuildContext context, SwiperPluginConfig config) {
                  List<Widget> pointChilds = List.generate(length, (index) {
                    bool _isSelected = index == config.activeIndex;
                    return Container(
                      width: _isSelected ? 10 : 5,
                      height: 5,
                      margin: EdgeInsets.all(2.5),
                      decoration: BoxDecoration(
                        color: _isSelected
                            ? Color(0xD8D8D8)
                            : Color(0xD8D8D8).withOpacity(0.41),
                        borderRadius: BorderRadius.circular(2.5),
                      ),
                    );
                  });
                  return ConstrainedBox(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: pointChilds,
                      ),
                    ),
                    constraints: BoxConstraints.expand(height: 10),
                  );
                })),
            onTap: (index) {},
          ),
        );
      },
    );
  }
}
