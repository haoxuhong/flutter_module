import 'package:dio/dio.dart';
import 'package:flutter_wisdomscenic/mvvm/base_view_model.dart';
import 'package:flutter_wisdomscenic/mvvm/imvvm.dart';

import '../net/dio_utils.dart';
import '../net/error_handle.dart';

class BasePageViewModel<V extends IMvvmView> extends BaseViewModel<V> {
  late CancelToken _cancelToken;

  BasePageViewModel() {
    _cancelToken = CancelToken();
  }

  @override
  void dispose() {
    /// 销毁时，将请求取消
    if (!_cancelToken.isCancelled) {
      _cancelToken.cancel();
    }
  }

  /// 接口请求例子
  /// get和delete方式请求参数queryParameters  post请求参数params
  ///   final Map<String, String> params = <String, String>{};
  ///    params['q'] = text;
  /*
  使用说明：
  1。响应值data类型为list时：
   例子：
      List<MineOrderRefundDetailBean> list = [];
      List dataList = data as List;
      for (int i = 0; i < dataList.length; i++) {
        list.add(MineOrderRefundDetailBean.fromJson(dataList[i]));
      }

   2。响应值data类型为bool时：
   例子：
    Records bean = Records.fromJson(data as Map<String, dynamic>);
   3。响应值data类型为map时：
   例子：
   view.cancelOrderSuccess(data ?? false);
  * */
  void asyncRequestNetwork<T>(
    Method method, {
    required String url,
    bool isShow = true,
    bool isClose = true,
    NetSuccessCallback<T?>? onSuccess,
    NetErrorCallback? onError,
    dynamic params,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelToken,
    Options? options,
  }) {
    if (isShow) {
      view.showProgress();
    }
    DioUtils.instance.asyncRequestNetwork<T>(
      method,
      url,
      params: params,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken ?? _cancelToken,
      onSuccess: (data) {
        if (isClose) view.closeProgress();
        onSuccess?.call(data);
      },
      onError: (code, msg) {
        _onError(code, msg, onError);
      },
    );
  }

  void _onError(int code, String? msg, NetErrorCallback? onError) {
    /// 异常时直接关闭加载圈，不受isClose影响
    view.closeProgress();
    if (code != ExceptionHandle.cancel_error) {
      view.showToast(msg);
    }

    /// 页面如果dispose，则不回调onError
    if (onError != null) {
      onError(code, msg);
    }
  }
}
