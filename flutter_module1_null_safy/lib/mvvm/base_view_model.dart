import 'package:flutter_wisdomscenic/mvvm/i_lifecycle.dart';
import 'package:flutter_wisdomscenic/mvvm/imvvm.dart';

class BaseViewModel<V extends IMvvmView> extends ILifecycle {
  late V view;

  @override
  void deactivate() {}

  @override
  void didChangeDependencies() {}

  @override
  void didUpdateWidgets<W>(W oldWidget) {}

  @override
  void dispose() {}

  @override
  void initState() {}
}
