import 'package:dio/dio.dart';
import 'package:flutter_wisdomscenic/common/constant.dart';
import 'package:hxh_base/util/device_utils.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/utils.dart';
import 'package:sp_util/sp_util.dart';
import 'package:sprintf/sprintf.dart';

import 'error_handle.dart';

class AuthInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    final String accessToken = SpUtil.getString(Constant.accessToken)!;
    if (accessToken.isNotEmpty) {
      options.headers[Constant.accessToken] = 'Bearer ' + accessToken;
    }
    // final String tenant = SpUtil.getString(Constant.tenant);
    final String tenant = '0000';
    if (tenant.isNotEmpty) {
      options.headers[Constant.tenant] = tenant;
    }
    if (!Device.isWeb) {
      // https://developer.github.com/v3/#user-agent-required
      options.headers['User-Agent'] = 'Mozilla/5.0';
    }

    options.headers['appAgent'] = Device.isAndroid ? 'APP_ANDROID' : 'APP_IOS';
    super.onRequest(options, handler);
  }
}

class LoggingInterceptor extends Interceptor {
  late DateTime _startTime;
  late DateTime _endTime;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    _startTime = DateTime.now();
    MyLogUtil.d('----------Start----------');
    if (options.queryParameters.isEmpty) {
      MyLogUtil.d('RequestUrl: ' + options.baseUrl + options.path);
    } else {
      MyLogUtil.d('RequestUrl: ' +
          options.baseUrl +
          options.path +
          '?' +
          Transformer.urlEncodeMap(options.queryParameters));
    }
    MyLogUtil.d('RequestMethod: ' + options.method);
    MyLogUtil.d('RequestHeaders:' + options.headers.toString());
    MyLogUtil.d('RequestContentType: ${options.contentType}');
    MyLogUtil.d('RequestData: ${options.data.toString()}');

    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    _endTime = DateTime.now();
    final int duration = _endTime.difference(_startTime).inMilliseconds;
    if (response.statusCode == ExceptionHandle.success) {
      MyLogUtil.d('ResponseCode: ${response.statusCode}');
    } else {
      MyLogUtil.e('ResponseCode: ${response.statusCode}');
    }
    // 输出结果
    // MyLogUtil.json(response.data.toString());
    MyLogUtil.d('----------End: $duration 毫秒----------');
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    MyLogUtil.d('----------Error-----------');
    super.onError(err, handler);
  }
}

class AdapterInterceptor extends Interceptor {
  static const String _kDefaultText = '无返回信息';
  static const String _kNotFound = '请求未知异常'; //404

  static const String _kFailureFormat =
      '{\"${Constant.code}\":%d,\"${Constant.message}\":\"%s\"}';

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (err.response != null) {
      adapterData(err.response!);
    }
    super.onError(err, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    final Response r = adapterData(response);
    super.onResponse(r, handler);
  }

  Response adapterData(Response response) {
    String content = response.data?.toString() ?? '';
    // content = '{\"${Constant.code}\":\"401\",\"${Constant.message}\":\"cuowuerror\"}'; //一种类型：401（token失效单独处理）
    // content = '{\"${Constant.code}\":\"500\",\"${Constant.message}\":\"cuowuerror\"}';//一种类型非：401
    // content = '';//一种类型：空  ''
    // content = '<html><body><script defer src="//hectorstatic.baidu.com/cd37ed75a9387c5b.js"></script></body></html>';
    ///0 成功时，直接格式化返回
    if (response.statusCode == ExceptionHandle.success) {
      ///后台没有返回值
      if (content.isEmpty) {
        response.data = sprintf(
            _kFailureFormat, [ExceptionHandle.empty_error, _kDefaultText]);
        return response;
      } else {
        final Map<String, dynamic>? jsonParse = Utils.jsonParse(content);
        int code = (jsonParse != null ? jsonParse[Constant.code] : -1);
        // String codeStr = Utils.int2Str(code);

        ///系统繁忙，请稍后重试
        if (code < 0) {
          content = sprintf(_kFailureFormat, [code, '系统繁忙，请稍后重试']);
        }

        ///401
        if (code == 40008 ||
            code == 40000 ||
            code == 40001 ||
            code == 40002 ||
            code == 40003 ||
            code == 40004 ||
            code == 40005) {
          MyLogUtil.d('-----------跳转到登录页codeStr：$code------------');

          /// todo 跳转到登录界面   全局context
          /*  Future.delayed(const Duration(microseconds: 0), () {
            NavigatorUtils.push(navigatorKey.currentState.overlay.context,
                LoginRouter.loginPage,
                replace: true);
          });*/
          if (code != 40008) {
            content = sprintf(
                _kFailureFormat, [ExceptionHandle.unauthorized, '会话失效']);
          }
        }
        response.data = content;
        return response;
      }
    }

    ///404
    // if (response.statusCode == ExceptionHandle.not_found) {
    else {
      /// 错误数据格式化后，按照成功数据返回
      response.data =
          sprintf(_kFailureFormat, [response.statusCode, _kNotFound]);
      response.statusCode = ExceptionHandle.success;
      return response;
    }
  }
}
