import 'package:flutter_wisdomscenic/common/constant.dart';
import 'package:sp_util/sp_util.dart';

class SpUtils {
  ///退出时删除sp里面存储的数据
  static void exitLoginRemoveSp() {
    SpUtil.remove(Constant.accessToken); //token删除
    SpUtil.remove(Constant.SP_TOKEN_EXPIRE_TIME); //token过期时间
  }
}
