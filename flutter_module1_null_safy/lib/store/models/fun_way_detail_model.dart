import 'package:flutter/material.dart';

///门票--门票详情--门票
class FunWayDetailModel extends ChangeNotifier {
  ///初始化数据
  void init() {
    _currentSelectedIndex = 0;
  }

  int _currentSelectedIndex = 0;

  int get currentSelectedIndex => _currentSelectedIndex;

  setCurrentSelectedIndex(int index) {
    _currentSelectedIndex = index;
    notifyListeners();
  }

}