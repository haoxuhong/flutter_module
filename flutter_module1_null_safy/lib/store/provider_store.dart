import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'index.dart';

class MyStore {
  //  初始化
  static init({Widget? child}) {
    /// 返回多个状态
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => FunWayDetailModel()),
      ],
      child: child,
    );
  }
}
