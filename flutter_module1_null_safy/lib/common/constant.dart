import 'package:flutter/foundation.dart';

class Constant {
  /// debug开关，上线需要关闭
  /// App运行在Release环境时，inProduction为true；当App运行在Debug和Profile环境时，inProduction为false
  static const bool inProduction = kReleaseMode;
  static const int doubleTapDurationTime = 2500; //双击间隔时间
  static const int toastDefaultShowTime = 2000; //toast显示时间
  static int pageNumber = 1; //分页起始页
  static const int pageSize = 10; // 一页的数量，默认为10
  static const String installName = '智慧景区'; //安装目录名

  static const String data = 'data';
  static const String message = 'msg';
  static const String code = 'code';

  static const String accessToken = 'token';
  static const String tenant = 'tenant';
  static const String SP_USER_ID = "userId"; //用户ID
  static const String SP_NAME_USER_ACCOUNT = "uweicAccount"; //用户账号
  static const String SP_TOKEN_EXPIRE_TIME = "tokenExpireTime"; //用户相关的SP 过期时间

  static const String baseUrl = 'http://www.baidu.com'; //测试

  static const String upload = 'xxxx'; //线上华为云链接

}
