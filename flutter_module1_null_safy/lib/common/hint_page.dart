import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/widgets/load_image.dart';
import 'package:flutter_wisdomscenic/widgets/my_app_bar.dart';
import 'package:hxh_base/res/resources.dart';

class HintPage extends StatelessWidget {
  final String? title;
  final String? hintText;

  const HintPage({Key? key, this.hintText, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        gradientStart: Colours.color_app_bar_gradient_start,
        gradientEnd: Colours.color_app_bar_gradient_end,
        centerTitle: title ?? '',
        titleColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              LoadAssetImage(
                'icon_hint_empty',
                width: radiusDimens(60),
                height: radiusDimens(60),
              ),
              hxhSpace(height: 10),
              Text(
                hintText!.isEmpty ? '该功能开发中，敬请期待...' : hintText!,
                style: TextStyles.textSBlack15,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
