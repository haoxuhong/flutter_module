import 'package:flutter/material.dart';

import 'fluro_navigator.dart';
import 'routers.dart';

class MyNavigatorUtils {
  /// 跳到hintpage页
  static void goHintPage(BuildContext context, String title,
      {String hintText: ''}) {
    //fluro 不支持传中文,需转换
    NavigatorUtils.push(context,
        '${Routes.hintPage}?title=${Uri.encodeComponent(title)}&hintText=${Uri.encodeComponent(hintText)}');
  }

  /// 跳到WebView页
  static void goWebViewPage(BuildContext context, String title, String url) {
    //fluro 不支持传中文,需转换
    NavigatorUtils.push(context,
        '${Routes.webViewPage}?title=${Uri.encodeComponent(title)}&url=${Uri.encodeComponent(url)}');
  }
}
