import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/mvvm/example/test_page.dart';

///随行
class GoAlongPage extends StatefulWidget {
  @override
  _GoAlongPageState createState() => _GoAlongPageState();
}

class _GoAlongPageState extends State<GoAlongPage>
    with
        AutomaticKeepAliveClientMixin<GoAlongPage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
            child: InkWell(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (content) => TestPage()));
                },
                child: Text('随行'))),
      ),
    );
  }
}
