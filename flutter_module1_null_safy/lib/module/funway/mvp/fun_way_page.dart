import 'package:flutter/material.dart';

class FunWayPage extends StatefulWidget {
  @override
  _FunWayPageState createState() => _FunWayPageState();
}

class _FunWayPageState extends State<FunWayPage>
    with
        AutomaticKeepAliveClientMixin<FunWayPage>,
        SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Center(child: Text('趣途')),
      ),
    );
  }
}
