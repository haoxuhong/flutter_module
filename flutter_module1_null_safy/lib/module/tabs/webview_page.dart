import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/routers/fluro_navigator.dart';
import 'package:flutter_wisdomscenic/routers/routers.dart';
import 'package:flutter_wisdomscenic/widgets/my_app_bar.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  const WebViewPage({
    Key? key,
    required this.title,
    required this.url,
  }) : super(key: key);

  final String? title;
  final String? url;

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<WebViewController>(
        future: _controller.future,
        builder: (context, snapshot) {
          return WillPopScope(
            onWillPop: () async {
              if (snapshot.hasData) {
                final bool canGoBack = await snapshot.data!.canGoBack();
                if (canGoBack) {
                  // 网页可以返回时，优先返回上一页
                  await snapshot.data!.goBack();
                  return Future.value(false);
                }
              }
              return Future.value(true);
            },
            child: Scaffold(
              appBar: MyAppBar(
                centerTitle: widget.title!,
                isWhiteStatusBarFont: false,
                actions: <Widget>[
                  IconButton(
                      icon: Container(
                          width: widthDimens(42),
                          height: heightDimens(18),
                          color: Colours.color_ticket_red,
                          alignment: Alignment.center,
                          child: Text('举报', style: TextStyles.textWhite8)),
                      onPressed: () {
                        NavigatorUtils.push(context, Routes.reportPage);
                      })
                ],
              ),
              body: WebView(
                initialUrl: widget.url,
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
              ),
            ),
          );
        });
  }
}
