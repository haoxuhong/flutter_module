import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/module/funway/mvp/fun_way_page.dart';
import 'package:flutter_wisdomscenic/module/goalong/mvp/goalong_page.dart';
import 'package:flutter_wisdomscenic/module/mine/mvp/mine_page.dart';
import 'package:flutter_wisdomscenic/module/tour/mvp/tour_page.dart';
import 'package:flutter_wisdomscenic/res/colors.dart';
import 'package:flutter_wisdomscenic/res/strings.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/double_tap_back_exit_app.dart';
import 'package:hxh_base/util/image_utils.dart';

class TabsPage extends StatefulWidget {
  @override
  _TabsPageState createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  final _pageList = [
    GoAlongPage(),
    FunWayPage(),
    TourPage(),
    MinePage(),
  ];

  final PageController _pageController = PageController();

  int currentIndex = 0;

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DoubleTapBackExitApp(
      child: Scaffold(
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.white,
            items: _buildBottomNavigationBarItem(),
            type: BottomNavigationBarType.fixed,
            currentIndex: currentIndex,
            selectedFontSize: fontSp(10),
            unselectedFontSize: fontSp(10),
            selectedItemColor: MyColors.menu_color_text_selected,
            unselectedItemColor: MyColors.menu_color_text_unselected,
            onTap: _onTap,
          ),
          // 使用PageView的原因参看 https://zhuanlan.zhihu.com/p/58582876
          body: PageView(
            physics: const NeverScrollableScrollPhysics(), // 禁止滑动
            controller: _pageController,
            onPageChanged: _onPageChanged,
            children: _pageList,
          )),
    );
  }

  void _onTap(int index) {
    _pageController.jumpToPage(index);
  }

  void _onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  ///tabbar图片抽取
  Widget _tabImage(String imageUrl) {
    return Container(
      margin: EdgeInsets.only(top: heightDimens(5)),
      child: Image.asset(
        ImageUtils.getImgPath(imageUrl),
        width: widthDimens(25),
        height: heightDimens(20),
        fit: BoxFit.scaleDown,
      ),
    );
  }

  List<BottomNavigationBarItem> _buildBottomNavigationBarItem() {
    List<String> _appBarLabels = [
      S.goAlong,
      S.funWay,
      S.tour,
      S.mine,
    ];
    List _tabImages = [
      [
        _tabImage(S.goAlong_image_url_unselected),
        _tabImage(S.goAlong_image_url_selected),
      ],
      [
        _tabImage(S.funWay_image_url_unselected),
        _tabImage(S.funWay_image_url_selected),
      ],
      [
        _tabImage(S.tour_image_url_unselected),
        _tabImage(S.tour_image_url_selected),
      ],
      [
        _tabImage(S.mine_image_url_unselected),
        _tabImage(S.mine_image_url_selected),
      ]
    ];
    List<BottomNavigationBarItem> _list = List.generate(_tabImages.length, (i) {
      return BottomNavigationBarItem(
        icon: _tabImages[i][0],
        activeIcon: _tabImages[i][1],
        label: _appBarLabels[i],
        // title: _appBarTitles[i],
      );
    });
    return _list;
  }
}
