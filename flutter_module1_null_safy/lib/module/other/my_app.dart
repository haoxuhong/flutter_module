import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_wisdomscenic/common/app_manager.dart';
import 'package:flutter_wisdomscenic/common/not_found_page.dart';
import 'package:flutter_wisdomscenic/module/tabs/tabs_page.dart';
import 'package:flutter_wisdomscenic/routers/routers.dart';
import 'package:flutter_wisdomscenic/routers/web_page_transitions.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:oktoast/oktoast.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  MyApp() {
    AppManager.initApp();
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    //设置适配尺寸 (填入设计稿中设备的屏幕尺寸) 此处假如设计稿是按iPhone6的尺寸设计的(iPhone6 750*1334)

    return ScreenUtilInit(
      designSize: const Size(375, 667),
      builder: (context, child) {
        return OKToast(
            child: _buildMaterialApp(),

            /// Toast 配置
            /// 全局设置隐藏之前的属性,这里设置后,每次当你显示新的 toast 时,旧的就会被关闭
            dismissOtherOnShow: true,
            //自己添加的设置，Flutter Deer开源项目没设置这个字段
            backgroundColor: Colors.black54,
            textPadding: EdgeInsets.symmetric(
                horizontal: widthDimens(16.0), vertical: heightDimens(10.0)),
            radius: radiusDimens(20),
            position: ToastPosition.bottom);
      },
    );
  }

  MaterialApp _buildMaterialApp() {
    return MaterialApp(
      title: '智慧景区',
//              showPerformanceOverlay: true, //显示性能标签
      // 去除右上角debug的标签
      debugShowCheckedModeBanner: false,

//              checkerboardRasterCacheImages: true,
//              showSemanticsDebugger: true, // 显示语义视图
//              checkerboardOffscreenLayers: true, // 检查离屏渲染
      theme: ThemeData(
        primaryColor: Colours.app_main,
        pageTransitionsTheme: NoTransitionsOnWeb(),
        visualDensity: VisualDensity
            .standard, // https://github.com/flutter/flutter/issues/77142
      ),

      // home: LoginPage(),
      home: TabsPage(),
      onGenerateRoute: Routes.router.generator,
      navigatorKey: navigatorKey,
      builder: (BuildContext context, Widget? child) {
        /// 保证文字大小不受手机系统设置影响 https://www.kikt.top/posts/flutter/layout/dynamic-text/
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: child!,
        );
      },

      /// 因为使用了fluro，这里设置主要针对Web
      onUnknownRoute: (_) {
        return MaterialPageRoute<void>(
          builder: (BuildContext context) => const NotFoundPage(),
        );
      },
      restorationScopeId: 'app',
    );
  }
}
