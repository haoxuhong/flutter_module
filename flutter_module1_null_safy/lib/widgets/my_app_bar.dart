import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/utils.dart';
import 'package:hxh_base/util/view_util.dart';
import 'package:hxh_base/widgets/image_load_view.dart';

/// 自定义AppBar
class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MyAppBar(
      {Key? key,
      this.backgroundColor: Colors.white,
      this.gradientStart,
      this.gradientEnd,
      this.isOrientationLeftRight = false,
      this.titleColor = Colours.color_black,
      this.titleFontSize,
      this.title: '',
      this.centerTitle: '',
      this.actions,
      this.backImg: 'ic_arrow_left_white',
      this.isBack: true,
      this.isCustomBack: false,
      this.isWhiteStatusBarFont: true,
      this.lineColor: Colours.color_gray_EEE,
      this.lineWidth: 0.5,
      this.haveLine = false,
      this.onPressedBack})
      : super(key: key);

  ///标题
  final Color? gradientStart; //渐变开始颜色
  final Color? gradientEnd; //渐变结束颜色
  final bool isOrientationLeftRight; //默认是从上到下

  final Color backgroundColor;
  final Color titleColor;
  final double? titleFontSize;
  final String title;
  final String centerTitle;
  final Color lineColor;
  final double lineWidth;
  final bool haveLine;

  ///左边按钮
  final String backImg;
  final bool isBack;
  final bool isCustomBack;
  final VoidCallback? onPressedBack;

  ///右边
  final List<Widget>? actions;

  ///状态栏字体颜色
  final bool isWhiteStatusBarFont;

  @override
  Widget build(BuildContext context) {
    /*  final SystemUiOverlayStyle _overlayStyle =
        ThemeData.estimateBrightnessForColor(_backgroundColor) ==
                Brightness.dark
            ? SystemUiOverlayStyle.light
            : SystemUiOverlayStyle.dark;*/

    final Widget back = isBack
        ? IconButton(
            onPressed: () {
              if (isCustomBack) {
                onPressedBack!();
              } else {
                FocusManager.instance.primaryFocus?.unfocus();
                Navigator.maybePop(context);
              }
            },
            padding: EdgeInsets.only(
              left: widthDimens(30),
              top: heightDimens(10),
              bottom: heightDimens(10),
              right: widthDimens(10),
            ),
            icon: ImageLoadView(
              backImg,
              width: widthDimens(10),
              height: heightDimens(19),
              imageType: ImageType.assets,
            ),
          )
        : SizedBox.shrink();

    final Widget titleWidget = Container(
      alignment:
          Utils.isEmpty(centerTitle) ? Alignment.centerLeft : Alignment.center,
      width: Utils.width,
      margin: const EdgeInsets.symmetric(horizontal: 48.0),
      child: Text(
        title.isEmpty ? centerTitle : title,
        style: TextStyle(
            fontSize: titleFontSize ?? fontSp(22),
            // fontWeight: FontWeight.bold,
            color: titleColor),
      ),
    );

    /*
    final Widget action = actionName.isNotEmpty
        ? Positioned(
            right: 0.0,
            child: Theme(
              data: Theme.of(context).copyWith(
                buttonTheme: const ButtonThemeData(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  minWidth: 60.0,
                ),
              ),
              child: FlatButton(
                child: Text(actionName, key: const Key('actionName')),
                textColor: Colours.color_standard_black,
                highlightColor: Colors.transparent,
                onPressed: onPressed,
              ),
            ),
          )
        : Gaps.empty;*/
    final Widget action = !Utils.isEmpty(actions)
        ? Positioned(
            right: widthDimens(12),
            child: Container(
              alignment: Alignment.centerRight,
              child: Row(
                children: actions!,
              ),
            ))
        : SizedBox.shrink();

    Widget appBar = SafeArea(
      child: Stack(
        alignment: Alignment.centerLeft,
        children: <Widget>[titleWidget, back, action],
      ),
    );
    // 添加到build方法最后，return之前，通过使用decoration实现颜色的渐变(左右渐变)

    appBar = Container(
      decoration: BoxDecoration(
          gradient: (gradientStart != null && gradientEnd != null)
              ? hxhLinearGradient(
                  fromLeftToRight: isOrientationLeftRight,
                  colors: [gradientStart!, gradientEnd!])
              : null,
          border: haveLine
              ? borderLine(context, lineColor: lineColor, width: lineWidth)
              : null
          /*阴影设置
              boxShadow: [
                new BoxShadow(
                  color: Colors.grey[500],
                  blurRadius: 20.0,
                  spreadRadius: 1.0,
                )
              ]*/
          ),
      child: appBar,
    );
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: isWhiteStatusBarFont
          ? SystemUiOverlayStyle.light
          : SystemUiOverlayStyle.dark,
      child: Material(
        // 判断是否使用渐变色
        color: gradientStart != null && gradientEnd != null
            ? Colors.transparent
            : backgroundColor,
        child: appBar,
      ),
    );
  }

  @override
  // Size get preferredSize => Size.fromHeight(Dimens.common_title_size);
  Size get preferredSize =>
      Size.fromHeight(Utils.navigationBarHeight - Utils.topSafeHeight);
}
