import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/log_util.dart';
import 'package:hxh_base/util/toast_util.dart';
import 'package:hxh_dialog/address_dialog.dart';
import 'package:hxh_dialog/update_dialog.dart';

///dialog使用示例
class ExampleDialog {
  ///显示底部dialog
  static void showModalBottomSheetDialog(BuildContext context,
      {required Widget child,
      bool isScrollControlled = true,
      Color backgroundColor = Colors.white}) {
    showModalBottomSheet<void>(
      context: context,
      backgroundColor: backgroundColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radiusDimens(16)),
            topRight: Radius.circular(radiusDimens(16))),
      ),
      // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),

      /// 禁止拖动关闭
      // enableDrag: false,
      /// 使用true则高度不受16分之9的最高限制
      isScrollControlled: isScrollControlled,
      builder: (BuildContext context) {
        return child;
      },
    );
  }

  static void addressDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return AddressDialog(
              onSelected:
                  (province, provinceCode, city, cityCode, county, countyCode) {
                MyLogUtil.d(
                    '$province $provinceCode $city $cityCode $county $countyCode');
                ToastUtil.show(
                    '$province $provinceCode $city $cityCode $county $countyCode');
              },
              title: '选择地区',
              unselectedColor: Colors.blue);
        });
  }

  static void updateDialog(BuildContext context) {
    showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (_) => UpdateDialog(
              mDownloadUrl:
                  'https://54a0bf2343ff38bdc347780545bd8c9e.dd.cdntips.com/imtt.dd.qq.com/16891/apk/E664A57DD3EA0540676EFC830BFDDE97.apk',
              // "https://dldir1.qq.com/weixin/android/weixin7014android1660.apk",
              mVersionName: 'v1.0.0',
              mUpdateContent: "到底更新了啥\n到底更新了啥\n到底更新了啥\n到底更新了啥\n到底更新了啥",
            ));
  }
}
