import 'package:flutter_wisdomscenic/mvp/base_page_presenter.dart';
import 'package:flutter_wisdomscenic/net/net.dart';

import 'send_captcha_iView.dart';

///发送验证码
class SendCaptchaPresenter extends BasePagePresenter<SendCaptchaIMvpView> {
  void sendCaptcha(Map<String, dynamic> params) {
    asyncRequestNetwork<Map>(Method.get, queryParameters: params, url: '链接',
        onSuccess: (data) {
      view.sendCaptchaSuccess();
    }, onError: (_, __) {
      print('$_, $__');
    });
  }
}
