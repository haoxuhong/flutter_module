import 'package:flutter/material.dart';
import 'package:flutter_wisdomscenic/enum/enum.dart';
import 'package:flutter_wisdomscenic/res/colors.dart';
import 'package:flutter_wisdomscenic/res/strings.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/widgets/image_load_view.dart';

class ProgressView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: radiusDimens(24),
        height: radiusDimens(24),
        child: CircularProgressIndicator(
          strokeWidth: radiusDimens(2),
        ),
      ),
    );
  }
}

class StateLayout extends StatelessWidget {
  const StateLayout(this.type,
      {Key? key, this.hintText = S.hint_layout_error_request, this.onTap})
      : super(key: key);
  final StateType type;
  final GestureTapCallback? onTap;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case StateType.failNet:
        return Container(
          width: double.infinity,
          child: Material(
            color: Colors.white,
            child: InkWell(
              onTap: () {
                onTap!();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ImageLoadView(
                    'icon_hint_network',
                    width: radiusDimens(100),
                    height: radiusDimens(100),
                    imageType: ImageType.assets,
                  ),
                  hxhSpace(height: 10),
                  Text(
                    S.hint_layout_error_network,
                    style: TextStyles.textSBlack15,
                  ),
                  hxhSpace(height: 5),
                  Text(
                    S.click_me_to_retry,
                    style: TextStyles.textSBlack15,
                  ),
                ],
              ),
            ),
          ),
        );
      case StateType.fail:
        return Container(
          width: double.infinity,
          child: Material(
            color: Colors.white,
            child: InkWell(
              onTap: () {
                onTap!();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ImageLoadView(
                    'icon_hint_request',
                    width: radiusDimens(100),
                    height: radiusDimens(100),
                    imageType: ImageType.assets,
                  ),
                  hxhSpace(height: 10),
                  Text(
                    hintText,
                    style: TextStyles.textSBlack15,
                  ),
                  hxhSpace(height: 5),
                  Text(
                    S.click_me_to_retry,
                    style: TextStyles.textSBlack15,
                  ),
                ],
              ),
            ),
          ),
        );
      case StateType.loading:
        return Container(
          alignment: Alignment.center,
          color: MyColors.bg_gray,
          child: ProgressView(),
        );
      case StateType.empty:
        return Container(
          width: double.infinity,
          child: Material(
            color: Colors.white,
            child: InkWell(
              onTap: () {
                onTap!();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ImageLoadView(
                    'icon_hint_empty',
                    width: radiusDimens(100),
                    height: radiusDimens(100),
                    imageType: ImageType.assets,
                  ),
                  hxhSpace(height: 10),
                  Text(
                    S.hint_layout_no_data,
                    style: TextStyles.textSBlack15,
                  ),
                  hxhSpace(height: 5),
                  Text(
                    S.click_me_to_retry,
                    style: TextStyles.textSBlack15,
                  ),
                ],
              ),
            ),
          ),
        );

      case StateType.notFoundPage:
        return Container(
          width: double.infinity,
          child: Material(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ImageLoadView(
                  'icon_hint_network',
                  width: radiusDimens(100),
                  height: radiusDimens(100),
                  imageType: ImageType.assets,
                ),
                hxhSpace(height: 10),
                Text(
                  hintText,
                  style: TextStyles.textSBlack15,
                ),
              ],
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
