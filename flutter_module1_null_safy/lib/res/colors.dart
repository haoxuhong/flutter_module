import 'package:flutter/material.dart';

class MyColors {
  static const Color app_main = Color(0xFF4688FA);
  static const Color bg_gray = Color(0xFFF4F4F4);
  static const Color menu_color_text_selected = Color(0xFFFF4F4F);
  static const Color menu_color_text_unselected = Color(0xFF999999);
}
