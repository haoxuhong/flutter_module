import 'package:flutter_wisdomscenic/mvp/bean/file_upload_entity.dart';

fileUploadEntityFromJson(FileUploadEntity data, Map<String, dynamic> json) {
	if (json['fullPath'] != null) {
		data.fullPath = json['fullPath'].toString();
	}
	if (json['fileName'] != null) {
		data.fileName = json['fileName'].toString();
	}
	if (json['filepath'] != null) {
		data.filepath = json['filepath'].toString();
	}
	if (json['bucketname'] != null) {
		data.bucketname = json['bucketname'].toString();
	}
	if (json['upload-date'] != null) {
		data.uploadDate = json['upload-date'].toString();
	}
	return data;
}

Map<String, dynamic> fileUploadEntityToJson(FileUploadEntity entity) {
	final Map<String, dynamic> data = new Map<String, dynamic>();
	data['fullPath'] = entity.fullPath;
	data['fileName'] = entity.fileName;
	data['filepath'] = entity.filepath;
	data['bucketname'] = entity.bucketname;
	data['upload-date'] = entity.uploadDate;
	return data;
}