import 'dart:convert';
import 'dart:math';
import 'dart:ui' as ui show window;

import 'package:common_utils/common_utils.dart';
import 'package:decimal/decimal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rational/rational.dart';

class Utils {
  /// 生成随机串
  ///
  /// [len] 字符串长度
  ///
  static String randomString({int len: 32}) {
    String character = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    String left = '';
    for (var i = 0; i < len; i++) {
      left = left + character[Random().nextInt(character.length)];
    }
    return left;
  }

  /// 检查对象或 List 或 Map 是否为空
  static bool isEmpty(Object? object) {
    if (object == null) return true;
    if (object is String && object.isEmpty) {
      return true;
    } else if (object is List && object.isEmpty) {
      return true;
    } else if (object is Map && object.isEmpty) {
      return true;
    }
    return false;
  }

  ///JSON数据解析
  static Map<String, dynamic>? jsonParse(String data) {
    return json.decode(data) as Map<String, dynamic>?;
  }

  ///例子： eyJwaG9uZU51bWJlciI6Ijk4OTIzNzkyMjkiLCJzbXNDb2RlIjoiMDkzNjYzIiwicGFzc3dkIjoiMTIzNDU2In0=
  static String jsonToBase64(String jsonStr) {
    return base64.encode(utf8.encode(jsonStr));
  }

  ///例子： {"phoneNumber":"9892379229","smsCode":"093663","passwd":"123456"}
  static String base64ToJson(String base64Str) {
    return utf8.decode(base64.decode(base64Str));
  }

  ///value: 文本内容；fontSize : 文字的大小；fontWeight：文字权重；maxWidth：文本框的最大宽度；maxLines：文本支持最大多少行 ；locale：当前手机语言；textScaleFactor：手机系统可以设置字体大小（默认1.0）
  static double calculateTextHeight(fontSize,
      {String value = '',
      FontWeight? fontWeight,
      double maxWidth = double.infinity,
      int maxLines = 1,
      double textScaleFactor = 1.0 //字体缩放大小
      }) {
    TextPainter painter = TextPainter(

        ///AUTO：华为手机如果不指定locale的时候，该方法算出来的文字高度是比系统计算偏小的。
        locale: WidgetsBinding.instance.window.locale,
        maxLines: maxLines,
        textDirection: TextDirection.ltr,
        textScaleFactor: textScaleFactor,
        text: TextSpan(
            text: value,
            style: TextStyle(
              fontWeight: fontWeight,
              fontSize: fontSize,
            )));
    painter.layout(maxWidth: maxWidth);

    ///文字的宽度:painter.width
    return painter.height;
  }

  /// 屏幕宽
  ///
  static double get width {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    return mediaQuery.size.width;
  }

  /// 屏幕高
  ///
  static double get height {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    return mediaQuery.size.height;
  }

  /// 标题栏高度（包括状态栏）
  ///
  static double get navigationBarHeight {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    return mediaQuery.padding.top + kToolbarHeight;
  }

  /// 状态栏高度
  ///
  static double get topSafeHeight {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    return mediaQuery.padding.top;
  }

  /// 底部状态栏高度
  ///
  static double get bottomSafeHeight {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    return mediaQuery.padding.bottom;
  }

  /// 复制到剪粘板
  ///
  static copyToClipboard(String text) {
    Clipboard.setData(new ClipboardData(text: text));
  }

  /// 隐藏键盘
  ///
  /// [context] 上下文
  ///
  static void hideKeyboard(BuildContext context) {
    // FocusScope.of(context).requestFocus(FocusNode());

    Future.delayed(Duration.zero).then((value) {
      FocusScopeNode _node = FocusScope.of(context);
      print('_node.hasFocus: ${_node.hasFocus}');
      // 根据键盘是否被拉起，来决定是否收起键盘
      if (_node.hasFocus) _node.requestFocus(FocusNode());
    });
  }

  /// 状态栏状态
  ///
  /// [enable] true为显示；false为隐藏
  ///
  static void statusBarEnable(bool enable) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: enable ? SystemUiOverlay.values : []);
  }

  ///价格格式化
  ///用到了common_utils开源库里面的东西
  static String formatPrice(String price,
      {MoneyFormat format = MoneyFormat.END_INTEGER}) {
    return MoneyUtil.changeYWithUnit(
        NumUtil.getDoubleByValueStr(price)!, MoneyUnit.YUAN,
        format: format);
  }

  ///去掉城市名字里面的市
  static String citySplit(String str) {
    if (str.isNotEmpty && str.endsWith('市')) {
      return str.split('市')[0];
    }
    return str;
  }

  //////////////////////类型转换start//////////////
  ///类型转换    json转Map
  static Map<String, dynamic>? jsonToMap(String data) {
    return json.decode(data) as Map<String, dynamic>?;
  }

  ///类型转换    map转Json
  static String mapToJson(Map<String, dynamic> data) {
    return json.encode(data);
  }

  ///类型转换    int--->string
  static String int2Str(int i) {
    return i.toString();
  }

  /// 类型转换    string--->int
  static int str2Int(String valueStr, {int defValue = 0}) {
    return int.tryParse(valueStr) ?? defValue;
  }

  /// 类型转换    double--->string   fractionDigits:后面小数点位数
  static String double2Str(double i, int fractionDigits) {
    return i.toStringAsFixed(fractionDigits);
  }

  ///类型转换    string--->double
  static double str2Double(String valueStr, {double defValue = 0}) {
    return double.tryParse(valueStr) ?? defValue;
  }

  /// 类型转换    double--->int
  static int double2Int(double d) {
    return d.toInt();
  }

  ///类型转换    int--->double
  static double int2Double(int i) {
    return i.toDouble();
  }

  ///isZero
  static bool isZero(num? value) {
    return value == null || value == 0;
  }

  /// 加 (精确相加,防止精度丢失).
  /// add (without loosing precision).
  static double add(num a, num b) {
    return (Decimal.parse(a.toString()) + Decimal.parse(b.toString()))
        .toDouble();
  }

  /// 减 (精确相减,防止精度丢失).
  /// subtract (without loosing precision).
  static double subtract(num a, num b) {
    return (Decimal.parse(a.toString()) - Decimal.parse(b.toString()))
        .toDouble();
  }

  /// 乘 (精确相乘,防止精度丢失).
  /// multiply (without loosing precision).
  static double multiply(num a, num b) {
    return (Decimal.parse(a.toString()) * Decimal.parse(b.toString()))
        .toDouble();
  }

  /// 除 (精确相除,防止精度丢失).
  /// divide (without loosing precision).
  static double divide(num a, num b) {
    Rational value = Decimal.parse(a.toString()) / Decimal.parse(b.toString());
    return value.toDecimal().toDouble();
  }

  /// 余数
  static Decimal remainder(num a, num b) {
    return Decimal.parse(a.toString()) % Decimal.parse(b.toString());
  }
//////////////////////类型转换end//////////////
}

/// String 空安全处理
extension StringExtension on String? {
  String get nullSafe => this ?? '';
}
