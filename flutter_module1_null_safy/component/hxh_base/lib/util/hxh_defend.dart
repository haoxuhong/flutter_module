import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

///异常统一处理
class HxhDefend {
  run(Widget app) {
    //框架异常
    FlutterError.onError = (FlutterErrorDetails details) async {
      //线上环境，走上报逻辑
      if (kReleaseMode) {
        if (details.stack != null) {
          Zone.current.handleUncaughtError(details.exception, details.stack!);
        }
        ErrorWidget.builder = (FlutterErrorDetails details) {
          return Scaffold(
            body: Center(
              child: Text("error:${details.exceptionAsString()}"),
            ),
          );
        };
      } else {
        //开发期间，走Console抛出
        FlutterError.dumpErrorToConsole(details);
      }
    };

    runZonedGuarded(() {
      runApp(app);
    }, (e, s) => _reportError(e, s));
  }

  ///通过接口上报异常
  _reportError(Object error, StackTrace s) {
    print('kReleaseMode:$kReleaseMode');
    print('catch error:$error');
  }
}
