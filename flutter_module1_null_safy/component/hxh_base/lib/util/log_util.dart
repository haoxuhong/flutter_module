///日志输出
class MyLogUtil {
  static const String _defTag = '###日志打印###';

  ///是否 debug   true为调试模式    false为开发模式
  static late bool _debug; //是否是debug模式
  static int _maxLen = 128;
  static String _tagValue = _defTag;

  static void init(
    bool isDebug, {
    String tag = _defTag,
    int maxLen = 128,
  }) {
    _tagValue = tag;
    _debug = isDebug;
    _maxLen = maxLen;
  }

  static void e(Object object, {String? tag}) {
    if (_debug) {
      _printLog(tag, ' e ', object);
    }
  }

  static void d(Object object, {String? tag}) {
    if (_debug) {
      _printLog(tag, ' d ', object);
    }
  }

  static void _printLog(String? tag, String stag, Object msg) {
    final StringBuffer sb = StringBuffer();
    sb.write(tag ?? _defTag);
    sb.write(stag);
    sb.write(msg);
    print(sb.toString());
  }

/*
 *
  static void json(String msg, {String tag = _defTag}) {
    if (_debug) {
      try {
        final dynamic data = convert.json.decode(msg);
        if (data is Map) {
          _printMap(data);
        } else if (data is List) {
          _printList(data);
        } else
          d(msg, tag: tag);
      } catch(ea) {
        e(msg, tag: tag);
      }
    }
  }

  // https://github.com/Milad-Akarie/pretty_dio_logger
  static void _printMap(Map data, {String tag = _defTag, int tabs = 1, bool isListItem = false, bool isLast = false}) {
    final bool isRoot = tabs == 1;
    final String initialIndent = _indent(tabs);
    tabs++;

    if (isRoot || isListItem) {
      d('$initialIndent{', tag: tag);
    }

    data.keys.toList().asMap().forEach((index, dynamic key) {
      final bool isLast = index == data.length - 1;
      dynamic value = data[key];
      if (value is String) {
        value = '\"$value\"';
      }
      if (value is Map) {
        if (value.isEmpty)
          d('${_indent(tabs)} $key: $value${!isLast ? ',' : ''}', tag: tag);
        else {
          d('${_indent(tabs)} $key: {', tag: tag);
          _printMap(value, tabs: tabs);
        }
      } else if (value is List) {
        if (value.isEmpty) {
          d('${_indent(tabs)} $key: ${value.toString()}', tag: tag);
        } else {
          d('${_indent(tabs)} $key: [', tag: tag);
          _printList(value, tabs: tabs);
          d('${_indent(tabs)} ]${isLast ? '' : ','}', tag: tag);
        }
      } else {
        final msg = value.toString().replaceAll('\n', '');
        d('${_indent(tabs)} $key: $msg${!isLast ? ',' : ''}', tag: tag);
      }
    });

    d('$initialIndent}${isListItem && !isLast ? ',' : ''}', tag: tag);
  }

  static void _printList(List list, {String tag = _defTag, int tabs = 1}) {
    list.asMap().forEach((i, dynamic e) {
      final bool isLast = i == list.length - 1;
      if (e is Map) {
        if (e.isEmpty) {
          d('${_indent(tabs)}  $e${!isLast ? ',' : ''}', tag: tag);
        } else {
          _printMap(e, tabs: tabs + 1, isListItem: true, isLast: isLast);
        }
      } else {
        d('${_indent(tabs + 2)} $e${isLast ? '' : ','}', tag: tag);
      }
    });
  }


  static String _indent([int tabCount = 1]) => '  ' * tabCount;



  static void _printLog(String tag, String stag, Object object) {
    String da = object.toString();
    tag = tag ?? _tagValue;
    if (da.length <= _maxLen) {
      print("$tag$stag $da");
      return;
    }
    print(
        '$tag$stag — — — — — — — — — — — — — — — — st — — — — — — — — — — — — — — — —');
    while (da.isNotEmpty) {
      if (da.length > _maxLen) {
        print("$tag$stag| ${da.substring(0, _maxLen)}");
        da = da.substring(_maxLen, da.length);
      } else {
        print("$tag$stag| $da");
        da = "";
      }
    }
    print(
        '$tag$stag — — — — — — — — — — — — — — — — ed — — — — — — — — — — — — — — — —');
  }*/
}
