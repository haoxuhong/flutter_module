import 'package:oktoast/oktoast.dart';

import '../constant.dart';

/// Toast工具类
class ToastUtil {
  static void show(String msg, {int duration = Constant.toastDefaultShowTime}) {
    showToast(msg,
        duration: Duration(milliseconds: duration), dismissOtherToast: true);
  }

  static void cancelToast() {
    dismissAllToast();
  }
}
