import 'package:flutter/material.dart';
import 'package:hxh_base/res/dimens.dart';

import 'colors.dart';

class TextStyles {
  static TextStyle tabTitleStyle =
      TextStyle(fontSize: fontSp(10), fontWeight: FontWeight.bold);
  static TextStyle textBold18 =
      TextStyle(fontSize: fontSp(18), fontWeight: FontWeight.bold);

  ///color_standard_black
  static TextStyle textSBlack7 = TextStyle(
    fontSize: fontSp(7),
    color: Colours.color_standard_black,
  );
  static TextStyle textSBlack9 = TextStyle(
    fontSize: fontSp(9),
    color: Colours.color_standard_black,
  );
  static TextStyle textSBlack10 = TextStyle(
    fontSize: fontSp(10),
    color: Colours.color_standard_black,
  );
  static TextStyle textSBlackWeight11 = TextStyle(
      fontSize: fontSp(11),
      color: Colours.color_standard_black,
      fontWeight: FontWeight.bold);
  static TextStyle textSBlack12 = TextStyle(
    fontSize: fontSp(12),
    color: Colours.color_standard_black,
  );

  static TextStyle textSBlack14 = TextStyle(
    fontSize: fontSp(14),
    color: Colours.color_standard_black,
  );
  static TextStyle textSBlackWeight14 = TextStyle(
      fontSize: fontSp(14),
      color: Colours.color_standard_black,
      fontWeight: FontWeight.bold);
  static TextStyle textSBlackWeight15 = TextStyle(
      fontSize: fontSp(15),
      color: Colours.color_standard_black,
      fontWeight: FontWeight.bold);
  static TextStyle textSBlack15 = TextStyle(
    fontSize: fontSp(15),
    color: Colours.color_standard_black,
  );
  static TextStyle textSBlack16 = TextStyle(
    fontSize: fontSp(16),
    color: Colours.color_standard_black,
  );
  static TextStyle textSBlackWeight17 = TextStyle(
      fontSize: fontSp(17),
      color: Colours.color_standard_black,
      fontWeight: FontWeight.bold);
  static TextStyle textSBlack18 = TextStyle(
    fontSize: fontSp(18),
    color: Colours.color_standard_black,
  );

  static TextStyle textSBlackWeight18 = TextStyle(
      fontSize: fontSp(18),
      color: Colours.color_standard_black,
      fontWeight: FontWeight.bold);

  static TextStyle textSBlack20 = TextStyle(
    fontSize: fontSp(20),
    color: Colours.color_standard_black,
  );

  ///black
  static TextStyle textBlack9 = TextStyle(
    fontSize: fontSp(9),
    color: Colors.black,
  );
  static TextStyle textBlackWeight9 = TextStyle(
      fontSize: fontSp(9), color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle textBlack10 = TextStyle(
    fontSize: fontSp(10),
    color: Colors.black,
  );
  static TextStyle textBlack12 = TextStyle(
    fontSize: fontSp(12),
    color: Colors.black,
  );
  static TextStyle textBlack13 = TextStyle(
    fontSize: fontSp(13),
    color: Colors.black,
  );

  static TextStyle textBlackWeight15 = TextStyle(
      fontSize: fontSp(15), color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle textBlackWeight17 = TextStyle(
      fontSize: fontSp(17), color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle textBlackWeight20 = TextStyle(
      fontSize: fontSp(20), color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle textBlackWeight21 = TextStyle(
      fontSize: fontSp(21), color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle textBlack14 = TextStyle(
    fontSize: fontSp(14),
    color: Colors.black,
  );
  static TextStyle textBlack17 = TextStyle(
    fontSize: fontSp(17),
    color: Colors.black,
  );
  static TextStyle textBlack20 = TextStyle(
    fontSize: fontSp(20),
    color: Colors.black,
  );

  ///white
  static TextStyle textWhite6 = TextStyle(
    fontSize: fontSp(6),
    color: Colors.white,
  );
  static TextStyle textWhite8 = TextStyle(
    fontSize: fontSp(8),
    color: Colors.white,
  );
  static TextStyle textWhite10 = TextStyle(
    fontSize: fontSp(10),
    color: Colors.white,
  );
  static TextStyle textWhite12 = TextStyle(
    fontSize: fontSp(12),
    color: Colors.white,
  );

  static TextStyle textWhite14 = TextStyle(
    fontSize: fontSp(14),
    color: Colors.white,
  );
  static TextStyle textWhite16 = TextStyle(
    fontSize: fontSp(16),
    color: Colors.white,
  );
  static TextStyle textWhite17 = TextStyle(
    fontSize: fontSp(17),
    color: Colors.white,
  );
  static TextStyle textWhite22 = TextStyle(
    fontSize: fontSp(22),
    color: Colors.white,
  );
  static TextStyle textWeightWhite17 = TextStyle(
      fontSize: fontSp(17), color: Colors.white, fontWeight: FontWeight.bold);
  static TextStyle textWeightWhite20 = TextStyle(
      fontSize: fontSp(20), color: Colors.white, fontWeight: FontWeight.bold);
  static TextStyle textWhite23 = TextStyle(
    fontSize: fontSp(23),
    color: Colors.white,
  );

  ///color_gray_999
  static TextStyle text9997 = TextStyle(
    fontSize: fontSp(7),
    color: Colours.color_gray_999,
  );
  static TextStyle text9999 = TextStyle(
    fontSize: fontSp(9),
    color: Colours.color_gray_999,
  );
  static TextStyle text99910 = TextStyle(
    fontSize: fontSp(10),
    color: Colours.color_gray_999,
  );
  static TextStyle text99912 = TextStyle(
    fontSize: fontSp(12),
    color: Colours.color_gray_999,
  );
  static TextStyle text99913 = TextStyle(
    fontSize: fontSp(13),
    color: Colours.color_gray_999,
  );
  static TextStyle text99914 = TextStyle(
    fontSize: fontSp(14),
    color: Colours.color_gray_999,
  );
  static TextStyle textWeight99915 = TextStyle(
      fontSize: fontSp(15),
      color: Colours.color_gray_999,
      fontWeight: FontWeight.bold);
  static TextStyle text99916 = TextStyle(
    fontSize: fontSp(16),
    color: Colours.color_gray_999,
  );
  static TextStyle text99918 = TextStyle(
    fontSize: fontSp(18),
    color: Colours.color_gray_999,
  );

  ///color_gray_666
  static TextStyle text6667 = TextStyle(
    fontSize: fontSp(7),
    color: Colours.color_gray_666,
  );
  static TextStyle text6610 = TextStyle(
    fontSize: fontSp(10),
    color: Colours.color_gray_666,
  );
  static TextStyle text6612 = TextStyle(
    fontSize: fontSp(12),
    color: Colours.color_gray_666,
  );
  static TextStyle text6614 = TextStyle(
    fontSize: fontSp(14),
    color: Colours.color_gray_666,
  );
  static TextStyle text6615 = TextStyle(
    fontSize: fontSp(15),
    color: Colours.color_gray_666,
  );
  static TextStyle text6618 = TextStyle(
    fontSize: fontSp(18),
    color: Colours.color_gray_666,
  );
}
