import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hxh_base/util/utils.dart';

///间距:适配大屏
SizedBox hxhSpace({double height: 1, double width: 1}) {
  return SizedBox(
      height: height.h, width: Utils.height > Utils.width ? width.w : width.h);
}

///高
double heightDimens(double height) {
  return height.h;
}

///宽:适配大屏
double widthDimens(double width) {
  return Utils.height > Utils.width ? width.w : width.h;
}

///弧角
double radiusDimens(double radius) {
  return radius.r;
}

///字体
double fontSp(double fontSize) {
  return fontSize.sp;
}
