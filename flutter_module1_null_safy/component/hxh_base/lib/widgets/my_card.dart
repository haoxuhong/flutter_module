import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

class MyCard extends StatelessWidget {
  const MyCard({Key? key, required this.child, this.color, this.shadowColor})
      : super(key: key);

  final Widget child;
  final Color? color;
  final Color? shadowColor;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: color ?? Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
              color: shadowColor ?? Colours.color_default_shadow,
              offset: const Offset(0.0, 2.0),
              blurRadius: 8.0,
              spreadRadius: 0.0),
        ],
      ),
      child: child,
    );
  }
}
