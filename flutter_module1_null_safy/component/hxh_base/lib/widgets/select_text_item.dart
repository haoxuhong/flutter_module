import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/widgets/image_load_view.dart';

/*使用示例
*  SelectTextItem(
                      leading: Icon(Icons.email, size: 26),
                      title: '邮箱',
                      subTitle: '当时发生的',
                      content: '${userModel.getEmail()}',
                      margin: EdgeInsets.only(left: 16.0, right: 8),
                      onTap: isEdit
                          ? () => pushNewPage(
                                  context,
                                  InputTextPage(
                                    title: '修改邮箱',
                                    content: '${userModel.getEmail()}',
                                    maxLines: 1,
                                    keyboardType: TextInputType.emailAddress,
                                  ), callBack: (value) {
                                userModel.setUser(email: value);
                              })
                          : null,
                      textAlign: TextAlign.right),
* */

///左边图片   左边文字和左边子标题  内容   右箭头（或图片）  下面分割线     点击事件（无点击事件时，隐藏箭头图标）
class SelectTextItem extends StatelessWidget {
  final GestureTapCallback? onTap;
  final String title;
  final String content;
  final TextAlign textAlign;
  final TextStyle? contentTextStyle;
  final Widget? leading;
  final IconData? trailing;
  final String subTitle;
  final double height;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? lineMargin;
  final Color? bgColor;
  final Color? lineColor;
  final String rightIconUrl;
  final double rightIconSize;
  final TextStyle? titleTextStyle;
  final TextStyle? subTitleTextStyle;
  final bool haveLine;

  const SelectTextItem({
    Key? key,
    this.onTap,
    required this.title,
    this.content: "",
    this.textAlign: TextAlign.end,
    this.contentTextStyle,
    this.leading,
    this.subTitle: "",
    this.height: 55.0,
    this.trailing,
    this.margin,
    this.lineMargin,
    this.bgColor,
    this.titleTextStyle,
    this.subTitleTextStyle,
    this.rightIconUrl: 'ic_arrow_right_gray',
    this.rightIconSize: 19,
    this.lineColor,
    this.haveLine: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: bgColor ?? Colors.white,
      child: InkWell(
          onTap: onTap,
          child: Container(
            height: height,
            width: double.infinity,
            child: Stack(
              children: [
                Container(
                    height: height,
                    margin: margin ??
                        EdgeInsets.symmetric(horizontal: widthDimens(15)),
                    child: Row(children: <Widget>[
                      Offstage(
                          offstage: leading == null,
                          child: Row(children: <Widget>[
                            leading == null ? SizedBox() : leading!,
                            hxhSpace(width: 16)
                          ])),
                      Expanded(
                        flex: 1,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                title,
                                style:
                                    titleTextStyle ?? TextStyles.textSBlack18,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Offstage(
                                  offstage: subTitle.isEmpty,
                                  child: hxhSpace(height: 9)),
                              Offstage(
                                  offstage: subTitle.isEmpty,
                                  child: Text(subTitle,
                                      style: subTitleTextStyle ??
                                          TextStyles.text99912,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis))
                            ]),
                      ),
                      Visibility(
                        visible: content.isNotEmpty,
                        child: Expanded(
                            flex: 2,
                            child: Padding(
                                padding: margin ??
                                    EdgeInsets.symmetric(
                                        horizontal: widthDimens(8)),
                                child: Text(content,
                                    maxLines: 1,
                                    textAlign: textAlign,
                                    overflow: TextOverflow.ellipsis,
                                    style: contentTextStyle ??
                                        TextStyles.text99912))),
                      ),
                      Offstage(
                        offstage: onTap == null,
                        child: ImageLoadView(
                          rightIconUrl,
                          imageType: ImageType.assets,
                          fit: BoxFit.scaleDown,
                          width: radiusDimens(rightIconSize),
                          height: radiusDimens(rightIconSize),
                        ),
                      )
                    ])),
                Visibility(
                  visible: haveLine,
                  child: Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      color: lineColor ?? Colours.color_gray_EEE,
                      height: 0.5,
                      margin: lineMargin ??
                          EdgeInsets.symmetric(horizontal: widthDimens(15)),
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
