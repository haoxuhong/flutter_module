import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

/// 不带卡片效果的 默认字号18，白字蓝底，高度64
///TextButton: 当 onPressed 不设置或者设置为 null 时，按钮为不可用状态。
class HxhMyButton extends StatelessWidget {
  const HxhMyButton({
    Key? key,
    this.text = '',
    this.fontSize,
    this.textColor,
    this.disabledTextColor,
    this.backgroundColor,
    this.disabledBackgroundColor,
    this.minHeight,
    this.minWidth = double.infinity,
    this.padding,
    this.radius = 0.0,
    this.side = BorderSide.none,
    required this.onPressed,
  }) : super(key: key);

  final String text;
  final double? fontSize;
  final Color? textColor;
  final Color? disabledTextColor;
  final Color? backgroundColor;
  final Color? disabledBackgroundColor;
  final double? minHeight;
  final double? minWidth;
  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final double radius;
  final BorderSide side;

  @override
  Widget build(BuildContext context) {
    return TextButton(
        child: Text(
          text,
          style: TextStyle(fontSize: fontSize ?? fontSp(24)),
        ),
        onPressed: onPressed,
        style: ButtonStyle(
          // 文字颜色
          foregroundColor: MaterialStateProperty.resolveWith(
            (states) {
              if (states.contains(MaterialState.disabled)) {
                return disabledTextColor ?? Colours.text_disabled;
              }
              return textColor ?? Colors.white;
            },
          ),
          // 背景颜色
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return disabledBackgroundColor ?? Colours.button_disabled;
            }
            return backgroundColor ?? Colours.app_main;
          }),
          // 水波纹
          overlayColor: MaterialStateProperty.resolveWith((states) {
            return (textColor ?? Colors.white).withOpacity(0.12);
          }),
          // 按钮最小大小
          minimumSize: (minWidth == null)
              ? null
              : MaterialStateProperty.all<Size>(
                  Size(minWidth!, minHeight ?? heightDimens(64))),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
              padding ?? EdgeInsets.symmetric(horizontal: widthDimens(16.0))),
          shape: MaterialStateProperty.all<OutlinedBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius),
            ),
          ),
          side: MaterialStateProperty.all<BorderSide>(side),
        ));
  }
}
