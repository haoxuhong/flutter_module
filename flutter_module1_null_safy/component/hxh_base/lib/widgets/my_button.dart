import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

///带卡片效果的
class MyButton extends StatelessWidget {
  const MyButton({
    Key? key,
    this.text = '',
    required this.onPressed,
    this.backgroundColor,
    this.textColor,
    this.textDisabledColor,
    this.buttonDisabledColor,
    this.height,
    this.minWidth,
    this.fontSize,
    this.padding,
    this.shape,
  }) : super(key: key);

  final String text;
  final Color? backgroundColor; //背景颜色
  final Color? textColor; //字体颜色
  final Color? textDisabledColor; //禁用状态下字体颜色
  final Color? buttonDisabledColor; //禁用状态下背景颜色
  final double? height; //高
  final double? minWidth; //宽
  final double? fontSize; //字体大小
  final EdgeInsetsGeometry? padding;
  final VoidCallback? onPressed;
  final ShapeBorder? shape;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: shape,
      height: height ?? heightDimens(48),
      minWidth: minWidth ?? widthDimens(50),
      padding: padding ?? EdgeInsets.symmetric(horizontal: widthDimens(10)),
      onPressed: onPressed,
      textColor: Colors.white,
      color: backgroundColor ?? Color(0xFFF4F4F4),
      disabledTextColor: textDisabledColor ?? Colours.text_disabled,
      disabledColor: buttonDisabledColor ?? Colours.button_disabled,
      //shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Text(
        text,
        style: TextStyle(
            color: textColor ?? Colors.white, fontSize: fontSize ?? fontSp(18)),
      ),
    );
  }
}

//  onHighlightChanged,        // 高亮变化的回调
//  textTheme,                 // 文字主题
//  textColor,                 // 文字颜色
//  disabledTextColor,         // 不可点击时文字颜色
//  color,                     // 背景色
//  disabledColor,             // 不可点击时背景色
//  highlightColor,            // 点击高亮时背景色
//  splashColor,               // 水波纹颜色
//  colorBrightness,
//  elevation,                 // 阴影高度
//  highlightElevation,        // 高亮时阴影高度
//  disabledElevation,         // 不可点击时阴影高度
//  padding,                   // 内容周围边距
//  shape,                     // 按钮样式
//  clipBehavior,  // 抗锯齿剪切效果
//  materialTapTargetSize,     // 点击目标的最小尺寸
//  animationDuration,         // 动画效果持续时长
//  minWidth,                  // 最小宽度
//  height,                    // 按钮高度
///没用到
Widget buildMaterialButton() {
  return MaterialButton(
    color: Colors.teal.withOpacity(0.4),
    height: 60.0,
    padding: EdgeInsets.symmetric(horizontal: 40),
    child: Text('MaterialButton'),
    onPressed: () {},
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(50.0))),
    clipBehavior: Clip.none,
  );
}
