import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/widgets/tabbar/hxh_underline_tab_indicator.dart';

///顶部tab切换组件
class HxhTab extends StatelessWidget {
  final List<Widget> tabs;
  final TabController controller;
  final bool isScrollable;

  final double? fontSize;
  final double? unselectedFontSize;
  final Color? labelColor;
  final Color unselectedLabelColor;

  final double underlineWith;
  final double underlineHeight;
  final EdgeInsetsGeometry underlineMargin;
  final StrokeCap strokeCap; //默认是圆形，矩形：StrokeCap.square

  final ValueChanged<int>? onTap;

  const HxhTab(this.tabs,
      {Key? key,
      required this.controller,
      this.fontSize,
      this.unselectedLabelColor = Colours.color_gray_999,
      this.labelColor,
      this.isScrollable = false,
      this.onTap,
      this.unselectedFontSize,
      this.underlineWith = 40,
      this.underlineHeight = 2,
      this.underlineMargin = EdgeInsets.zero,
      this.strokeCap = StrokeCap.round})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
        indicator: HxhUnderlineTabIndicator(
            insets: underlineMargin,
            strokeCap: strokeCap,
            width: widthDimens(underlineWith),
            borderSide: BorderSide(
                color: labelColor ?? Colours.app_main, width: underlineHeight)),
        controller: controller,
        isScrollable: isScrollable,
        labelColor: labelColor ?? Colours.app_main,
        unselectedLabelColor: unselectedLabelColor,
        labelPadding: EdgeInsets.zero,
        labelStyle: TextStyle(
            fontSize: fontSp(fontSize ?? 15), fontWeight: FontWeight.bold),
        unselectedLabelStyle:
            TextStyle(fontSize: fontSp(unselectedFontSize ?? 12)),
        onTap: onTap,
        tabs: tabs);
  }
}
