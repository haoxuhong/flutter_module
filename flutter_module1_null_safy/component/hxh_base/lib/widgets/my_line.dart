import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';

///水平方向的线
class MyHLine extends StatelessWidget {
  final Color? lineColor;
  final double? height;
  final double? width;

  const MyHLine({Key? key, this.lineColor, this.height, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? double.infinity,
      height: height ?? 1,
      color: lineColor ?? Colours.color_split_line,
    );
  }
}

///垂直方向的线
class MyVLine extends StatelessWidget {
  final Color? lineColor;
  final double? width;
  final double? height;

  const MyVLine({Key? key, this.lineColor, this.width, this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 1,
      height: height ?? double.infinity,
      color: lineColor ?? Colours.color_split_line,
    );
  }
}
