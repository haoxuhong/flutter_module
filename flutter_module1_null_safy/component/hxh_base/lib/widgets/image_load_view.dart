import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hxh_base/util/image_utils.dart';

/*例子：
  ImageLoadView(
            bean?.faceImgUrl ?? '',
            shape: BoxShape.circle,
            width: radiusDimens(63),
            height: radiusDimens(63),
            fit: BoxFit.scaleDown,
            radius: 3,
            placeholder: 'mine/ic_mine_avatar',
          ),
* */

///图片加载
class ImageLoadView extends StatelessWidget {
  /// 图片URL
  final String path;

  /// 宽
  final double? width;

  /// 高
  final double? height;

  /// 填充效果
  final BoxFit fit;

  /// 加载中图片
  final String placeholder;

  final ImageType imageType;

  /// 圆角半径
  final double radius;

  final BoxShape shape;

  final Color? borderColor;

  final double borderWidth;

  final ImageFormat format;

  ImageLoadView(
    this.path, {
    Key? key,
    this.radius: 0.0,
    this.width,
    this.height,
    this.fit: BoxFit.scaleDown,
    this.placeholder: "loading",
    this.imageType: ImageType.network,
    this.shape: BoxShape.rectangle,
    this.borderColor,
    this.borderWidth: 0.0,
    this.format: ImageFormat.png,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ImageProvider imageProvider;

    if (imageType == ImageType.network) {
      Widget imageWidget = shape == BoxShape.circle
          ? _buildCachedNetworkImageCircle()
          : _buildCachedNetworkImageRectangle();
      return SizedBox(
        height: height ?? double.infinity,
        width: width ?? double.infinity,
        child: imageWidget,
      );
    }
    if (imageType == ImageType.localFile) {
      imageProvider = FileImage(File(path));
    } else {
      imageProvider = AssetImage(ImageUtils.getImgPath(path, format: format));
    }
    return Container(
      height: height ?? double.infinity,
      width: width ?? double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: fit,
          ),
          shape: shape,
          borderRadius:
              shape == BoxShape.circle ? null : BorderRadius.circular(radius),
          border: Border.all(
              color: borderColor ?? Theme.of(context).primaryColor,
              width: borderWidth,
              style:
                  borderWidth == 0.0 ? BorderStyle.none : BorderStyle.solid)),
    );
  }

  ///网络图片---矩形
  CachedNetworkImage _buildCachedNetworkImageRectangle() {
    return CachedNetworkImage(
        imageUrl: path,
        imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: fit,
                  ),
                  shape: shape,
                  borderRadius: BorderRadius.circular(radius),
                  border: Border.all(
                      color: borderColor ?? Theme.of(context).primaryColor,
                      width: borderWidth,
                      style: borderWidth == 0.0
                          ? BorderStyle.none
                          : BorderStyle.solid)),
            ),
        placeholder: (context, url) =>
            Image.asset(ImageUtils.getImgPath(placeholder, format: format)),
        fit: fit,
        errorWidget: (context, url, error) => Container());
  }

  ///网络图片---圆形
  CachedNetworkImage _buildCachedNetworkImageCircle() {
    return CachedNetworkImage(
      imageUrl: path,
      placeholder: (context, url) => CircleAvatar(
        backgroundImage:
            AssetImage(ImageUtils.getImgPath(placeholder, format: format)),
      ),
      imageBuilder: (context, image) => CircleAvatar(
        backgroundImage: image,
      ),
      fit: fit,
      errorWidget: (context, url, error) => Container(),
    );
  }
}

enum ImageType { network, assets, localFile }
