import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HxhProviderStore {
  //  通过Provider.value<T>(context)获取状态数据
  //  listen: 为true时重新构建（build刷新界面）,其行为类似于watch;为false时不重新构建（不刷新界面）其行为类似于read
  static T value<T>(BuildContext context, {bool listen: true}) {
    return Provider.of<T>(context, listen: listen);
  }

  //  通过Consumer获取状态数据  widget build太频繁时使用
  //  内部使用的是Provider.of<T>(context, listen: true)
  static Consumer connect<T>(
      {required Function(BuildContext context, T value, Widget? child) builder,
      Widget? child}) {
    return Consumer<T>(
        builder: builder as Widget Function(BuildContext, T, Widget?),
        child: child);
  }

  //  通过Consumer获取状态数据
  static Consumer2 connect2<A, B>(
      {required Function(BuildContext context, A value, B value2, Widget child)
          builder,
      Widget? child}) {
    return Consumer2<A, B>(
        builder: builder as Widget Function(BuildContext, A, B, Widget?),
        child: child);
  }
}
