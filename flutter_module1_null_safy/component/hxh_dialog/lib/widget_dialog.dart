import 'package:flutter/material.dart';
import 'package:hxh_base/res/colors.dart';
import 'package:hxh_base/res/dimens.dart';
import 'package:hxh_base/widgets/image_load_view.dart';

///底部弹框标题
class BottomSheetTitleWidget extends StatelessWidget {
  final Function onTapDelete;

  final EdgeInsetsGeometry? padding;
  final String titleText;
  final AlignmentGeometry titleAlignment;
  final Color titleColor;
  final Color iconColor;
  final double fontSize;
  final FontWeight fontWeight;
  final bool? isImageHide;
  final double? iconSize;

  BottomSheetTitleWidget({
    Key? key,
    required this.onTapDelete,
    this.titleText = "",
    this.titleColor = Colours.color_black_11,
    this.fontSize = 14.0,
    this.fontWeight = FontWeight.bold,
    this.titleAlignment = Alignment.centerLeft,
    this.padding,
    this.isImageHide,
    this.iconColor = Colors.black,
    this.iconSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: heightDimens(69),
      width: double.infinity,
      // padding: EdgeInsets.only(top: heightDimens(18.5)),
      child: Stack(
        children: <Widget>[
          Container(
            alignment: titleAlignment,
            margin: EdgeInsets.only(top: heightDimens(18.5)),
            child: Text(
              titleText,
              style: TextStyle(
                color: titleColor,
                fontSize: fontSize,
                fontWeight: fontWeight,
              ),
            ),
          ),
          Positioned(
              right: widthDimens(14.5),
              top: heightDimens(12.5),
              child: InkWell(
                onTap: () => onTapDelete(),
                child: ImageLoadView(
                  'ic_close',
                  width: radiusDimens(25.5),
                  height: radiusDimens(25.5),
                  imageType: ImageType.assets,
                ),
              )),
        ],
      ),
    );
  }
}
