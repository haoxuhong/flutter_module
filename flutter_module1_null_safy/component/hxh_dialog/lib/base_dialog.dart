import 'package:flutter/material.dart';
import 'package:hxh_base/res/resources.dart';
import 'package:hxh_base/util/device_utils.dart';
import 'package:hxh_base/widgets/hxh_my_button.dart';
import 'package:hxh_base/widgets/my_line.dart';
import 'package:hxh_dialog/utils/dialog_util.dart';

/// 自定义dialog的模板  弹到中间，底部有取消和确定按钮
/// 使用实例：
/*
 showDialog<void>(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return BaseDialog(
                        title: '规格名称',
                        child:  Container(),
                        onPressed: () {
                          NavigatorUtils.goBack(context);
                          ToastUtil.show('点击了确认按钮');
                        },
                      );
                    },
                  );

*/
class BaseDialog extends StatelessWidget {
  const BaseDialog(
      {Key? key,
      this.title,
      this.onPressed,
      this.hiddenTitle = false,
      required this.child})
      : super(key: key);

  final String? title;
  final VoidCallback? onPressed;
  final Widget child;
  final bool hiddenTitle;

  @override
  Widget build(BuildContext context) {
    final Widget dialogTitle = Visibility(
      visible: !hiddenTitle,
      child: Text(
        hiddenTitle ? '' : title ?? '',
        style: TextStyles.textBold18,
      ),
    );

    final Widget bottomButton = Row(
      children: <Widget>[
        _DialogButton(
          text: '取消',
          textColor: Colours.color_gray_999,
          onPressed: () => DialogUtil.goBack(context),
        ),
        MyVLine(
          height: heightDimens(48.0),
        ),
        _DialogButton(
          text: '确定',
          textColor: Theme.of(context).primaryColor,
          onPressed: onPressed,
        ),
      ],
    );

    final Widget content = Material(
      borderRadius: BorderRadius.circular(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          hxhSpace(height: 24),
          dialogTitle,
          hxhSpace(height: 10),
          Flexible(child: child),
          hxhSpace(height: 8),
          MyHLine(),
          bottomButton,
        ],
      ),
    );

    final Widget body = MediaQuery.removeViewInsets(
      removeLeft: true,
      removeTop: true,
      removeRight: true,
      removeBottom: true,
      context: context,
      child: Center(
        child: SizedBox(
          width: widthDimens(270.0),
          child: content,
        ),
      ),
    );

    /// Android 11添加了键盘弹出动画，这与我添加的过渡动画冲突（原先iOS、Android 没有相关过渡动画，相关问题跟踪：https://github.com/flutter/flutter/issues/19279）。
    /// 因为在Android 11上，viewInsets的值在键盘弹出过程中是变化的（以前只有开始结束的值）。
    /// 所以解决方法就是在Android 11及以上系统中使用Padding代替AnimatedPadding。

    if (Device.getAndroidSdkInt() >= 30) {
      return Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: body,
      );
    } else {
      return AnimatedPadding(
        padding: MediaQuery.of(context).viewInsets,
        duration: const Duration(milliseconds: 120),
        curve: Curves.easeInCubic, // easeOutQuad
        child: body,
      );
    }
  }
}

class _DialogButton extends StatelessWidget {
  const _DialogButton({
    Key? key,
    required this.text,
    this.textColor,
    this.onPressed,
  }) : super(key: key);

  final String text;
  final Color? textColor;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: HxhMyButton(
        text: text,
        textColor: textColor,
        onPressed: onPressed,
        backgroundColor: Colors.transparent,
      ),
    );
  }
}
